-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: evans
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assetindexdata`
--

DROP TABLE IF EXISTS `assetindexdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text,
  `size` bigint(20) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assetindexdata_sessionId_volumeId_idx` (`sessionId`,`volumeId`),
  KEY `assetindexdata_volumeId_idx` (`volumeId`),
  CONSTRAINT `assetindexdata_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assetindexdata`
--

LOCK TABLES `assetindexdata` WRITE;
/*!40000 ALTER TABLE `assetindexdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `assetindexdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `kind` varchar(50) NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `focalPoint` varchar(13) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assets_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `assets_folderId_idx` (`folderId`),
  KEY `assets_volumeId_idx` (`volumeId`),
  CONSTRAINT `assets_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assets_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` VALUES (19,1,1,'grey-placeholder.jpg','image',284,190,1203,NULL,'2018-08-18 22:32:08','2018-08-18 22:32:08','2018-08-18 22:32:08','29017144-8d16-47c0-93da-e0f9850bb9fb'),(20,1,1,'black-placeholder.jpg','image',284,190,1761,NULL,'2018-08-18 22:32:12','2018-08-18 22:32:12','2018-08-18 22:32:12','b6ed0c34-2b67-4432-8a42-7110ad83c875'),(41,1,1,'black-portrait-placeholder.jpg','image',190,284,1220,NULL,'2018-08-21 08:14:51','2018-08-21 08:14:51','2018-08-21 08:14:51','c96fc6ab-997b-4876-8dbc-b366f03c2768'),(42,1,1,'grey-portrait-placeholder.jpg','image',190,284,1219,NULL,'2018-08-21 08:15:00','2018-08-21 08:15:00','2018-08-21 08:15:00','1f386e29-69d4-4668-95d6-4cf01e531f9c');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assettransformindex`
--

DROP TABLE IF EXISTS `assettransformindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assettransformindex_volumeId_assetId_location_idx` (`volumeId`,`assetId`,`location`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assettransformindex`
--

LOCK TABLES `assettransformindex` WRITE;
/*!40000 ALTER TABLE `assettransformindex` DISABLE KEYS */;
INSERT INTO `assettransformindex` VALUES (1,20,'black-placeholder.jpg',NULL,'_general',1,1,0,'2018-08-21 08:18:44','2018-08-21 08:18:44','2018-08-21 08:18:45','a801831d-aba9-421b-aa5e-6975da9e1b4a'),(2,19,'grey-placeholder.jpg',NULL,'_general',1,1,0,'2018-08-21 08:32:22','2018-08-21 08:32:22','2018-08-21 08:32:23','6cbacdad-0f9e-44f3-8ae6-3dcd628cd460'),(3,41,'black-portrait-placeholder.jpg',NULL,'_general',1,1,0,'2018-08-21 08:35:32','2018-08-21 08:35:32','2018-08-21 08:35:33','0dc2a8be-053e-4af2-84b1-9c26d5ae6a95'),(4,42,'grey-portrait-placeholder.jpg',NULL,'_general',1,1,0,'2018-08-21 08:35:32','2018-08-21 08:35:32','2018-08-21 08:35:33','47fcb6d3-8ad8-46c6-88e1-5e69665f4fee');
/*!40000 ALTER TABLE `assettransformindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assettransforms`
--

DROP TABLE IF EXISTS `assettransforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `mode` enum('stretch','fit','crop') NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') NOT NULL DEFAULT 'center-center',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assettransforms`
--

LOCK TABLES `assettransforms` WRITE;
/*!40000 ALTER TABLE `assettransforms` DISABLE KEYS */;
INSERT INTO `assettransforms` VALUES (1,'General','general','crop','center-center',1280,NULL,NULL,60,'none','2018-08-21 08:18:42','2018-08-21 08:18:42','2018-08-21 08:18:42','294a9d9e-d032-40f6-810d-1ae8eb53d4ee');
/*!40000 ALTER TABLE `assettransforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categories_groupId_idx` (`groupId`),
  CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorygroups`
--

DROP TABLE IF EXISTS `categorygroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `categorygroups_handle_unq_idx` (`handle`),
  KEY `categorygroups_structureId_idx` (`structureId`),
  KEY `categorygroups_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorygroups`
--

LOCK TABLES `categorygroups` WRITE;
/*!40000 ALTER TABLE `categorygroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorygroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorygroups_sites`
--

DROP TABLE IF EXISTS `categorygroups_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorygroups_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_sites_groupId_siteId_unq_idx` (`groupId`,`siteId`),
  KEY `categorygroups_sites_siteId_idx` (`siteId`),
  CONSTRAINT `categorygroups_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categorygroups_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorygroups_sites`
--

LOCK TABLES `categorygroups_sites` WRITE;
/*!40000 ALTER TABLE `categorygroups_sites` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorygroups_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_introduction` text,
  `field_telephone` text,
  `field_email` text,
  `field_year` text,
  `field_projectSummary` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `content_siteId_idx` (`siteId`),
  KEY `content_title_idx` (`title`),
  CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `content_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (1,1,1,NULL,'2018-08-16 22:24:19','2018-08-16 22:24:19','af1a78c7-3f55-4ccb-b505-1279ac658520',NULL,NULL,NULL,NULL,NULL),(2,2,1,'Index','2018-08-16 22:39:09','2018-08-17 06:02:39','043ab201-d78f-4104-931d-75db5d5b069a','Graphic Designer. \nFrom Wellington, New Zealand.\nBased in Oakland, California.','+1 (510) 265 9088','mail@jeremyevans.co',NULL,NULL),(3,3,1,'Other','2018-08-16 22:39:34','2018-08-17 00:48:38','d7d21749-2275-4ce5-a090-a4e014f2dce0',NULL,NULL,NULL,NULL,NULL),(4,4,1,'Index','2018-08-16 23:23:48','2018-08-16 23:23:48','acc9240c-c3f7-4df9-8a9e-75948e1e790e',NULL,NULL,NULL,NULL,NULL),(5,5,1,'Other','2018-08-16 23:23:59','2018-09-14 19:28:27','91ef7cbe-6874-46d9-9a36-bc7f8a43ebd3',NULL,NULL,NULL,NULL,NULL),(6,19,1,'Grey Placeholder','2018-08-18 22:32:08','2018-08-18 22:32:08','0621abaf-0864-4546-8898-9a01cb814a63',NULL,NULL,NULL,NULL,NULL),(7,20,1,'Black Placeholder','2018-08-18 22:32:12','2018-08-18 22:32:12','5a57179a-88a5-424a-9cd4-3ab74649d441',NULL,NULL,NULL,NULL,NULL),(8,21,1,'The International','2018-08-18 22:32:47','2018-08-21 08:15:58','183d7c0a-a9d0-49fe-acd5-8e067e4a94dc',NULL,NULL,NULL,'2016','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \n\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.'),(9,23,1,'The Conference Company','2018-08-18 22:32:57','2018-08-19 09:12:55','cda9c9f5-03a0-4d3f-80a4-bc09c15909dc',NULL,NULL,NULL,'2016','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \n\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.'),(10,25,1,'Edition','2018-08-18 22:33:35','2018-08-19 09:12:55','0acc7e68-96c7-490a-822a-ad34c2cb0f7e',NULL,NULL,NULL,'2016','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \n\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.'),(11,27,1,'At Your Request','2018-08-18 22:33:57','2018-08-19 09:12:55','312d9dca-c11e-44c1-8d5c-2fffb715438c',NULL,NULL,NULL,'2016','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \n\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.'),(12,29,1,'International Post','2018-08-18 22:34:21','2018-08-19 09:12:55','8f98d47b-8ea0-462f-938c-f239b36da934',NULL,NULL,NULL,'2016','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \n\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.'),(13,31,1,'Culprit','2018-08-18 22:34:53','2018-08-19 09:12:55','0e804152-8078-4093-bcde-7ab575e52cba',NULL,NULL,NULL,'2016','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \n\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.'),(14,33,1,'Batchwell','2018-08-18 22:35:10','2018-08-19 09:12:55','941701d7-44b2-4f77-bde0-7d4a83b774d7',NULL,NULL,NULL,'2016','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \n\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.'),(15,35,1,'Aalto Paints','2018-08-18 22:35:44','2018-08-19 09:12:55','6f52bc39-c088-40ca-a5b9-1111fb348149',NULL,NULL,NULL,'2016','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \n\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.'),(16,41,1,'Black Portrait Placeholder','2018-08-21 08:14:51','2018-08-21 08:14:51','f3d5f50d-a0cd-43e8-ae51-75b6c621a475',NULL,NULL,NULL,NULL,NULL),(17,42,1,'Grey Portrait Placeholder','2018-08-21 08:15:00','2018-08-21 08:15:00','851f4910-99f7-4597-a47d-9ff770ee35e6',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `craftidtokens`
--

DROP TABLE IF EXISTS `craftidtokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `craftidtokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `accessToken` text NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craftidtokens_userId_fk` (`userId`),
  CONSTRAINT `craftidtokens_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `craftidtokens`
--

LOCK TABLES `craftidtokens` WRITE;
/*!40000 ALTER TABLE `craftidtokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `craftidtokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deprecationerrors`
--

DROP TABLE IF EXISTS `deprecationerrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `traces` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deprecationerrors`
--

LOCK TABLES `deprecationerrors` WRITE;
/*!40000 ALTER TABLE `deprecationerrors` DISABLE KEYS */;
/*!40000 ALTER TABLE `deprecationerrors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elementindexsettings`
--

DROP TABLE IF EXISTS `elementindexsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elementindexsettings`
--

LOCK TABLES `elementindexsettings` WRITE;
/*!40000 ALTER TABLE `elementindexsettings` DISABLE KEYS */;
INSERT INTO `elementindexsettings` VALUES (1,'craft\\elements\\Entry','{\"sources\":{\"singles\":{\"tableAttributes\":{\"1\":\"link\",\"2\":\"slug\"}}}}','2018-08-17 00:37:36','2018-08-17 00:37:36','ff5ec859-50df-4d54-94b3-5fc22099fb94');
/*!40000 ALTER TABLE `elementindexsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements`
--

DROP TABLE IF EXISTS `elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `elements_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `elements_type_idx` (`type`),
  KEY `elements_enabled_idx` (`enabled`),
  KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`),
  CONSTRAINT `elements_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements`
--

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
INSERT INTO `elements` VALUES (1,NULL,'craft\\elements\\User',1,0,'2018-08-16 22:24:19','2018-08-16 22:24:19','4058dad7-2642-4af4-84c6-e6de44c2c983'),(2,1,'craft\\elements\\Entry',1,0,'2018-08-16 22:39:09','2018-08-17 06:02:39','c11b9bf7-74b6-4416-9c77-a785c0beb164'),(3,2,'craft\\elements\\Entry',1,0,'2018-08-16 22:39:34','2018-08-17 00:48:38','487df3f9-374d-4b41-a22f-c7ad38734fe8'),(4,3,'craft\\elements\\Entry',1,0,'2018-08-16 23:23:48','2018-08-16 23:23:48','2b664320-148c-4f75-b492-b21c129c28e8'),(5,3,'craft\\elements\\Entry',0,0,'2018-08-16 23:23:59','2018-09-14 19:28:27','51c65650-3329-4e06-b398-d1012e6cd2c5'),(6,4,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','56eb23b4-3a9d-4f92-b2f3-b6626bef8bdd'),(7,4,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','d008f946-779f-45c9-bf2c-26caa5393e01'),(8,4,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','a1087c71-4673-4461-b23e-6ec25c88e208'),(9,4,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','0e7a1fa5-ebfb-47d4-bbdd-26a0d51cabcf'),(10,5,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','e82a0a4c-1fd5-42af-ab83-c9b3a27ded77'),(11,5,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','d487515d-4135-4192-ae4d-5bc2f9a49fea'),(12,5,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','65d5a57f-4174-43a3-b923-358767181e7f'),(13,5,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','c40bc7ec-39db-45ca-b3bb-a6a57874fa04'),(14,5,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','297d4ba0-6bc4-42c7-b6c6-bf5d133faa7a'),(15,5,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','fa308a44-0a4d-4687-b1f1-f86d70c79852'),(16,5,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','503899c3-d53b-4ba6-81ec-a4454fd4490a'),(17,5,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','5c5c3601-d88a-45dc-908b-54c761af391b'),(18,5,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-17 01:03:14','2018-08-17 06:02:39','d340d7c0-21bc-4547-a977-8a54ae4fe824'),(19,8,'craft\\elements\\Asset',1,0,'2018-08-18 22:32:08','2018-08-18 22:32:08','fbaee965-b97a-4b13-a49d-fce1a830ec0a'),(20,8,'craft\\elements\\Asset',1,0,'2018-08-18 22:32:12','2018-08-18 22:32:12','3625a838-8164-42bb-9559-718d21e1b049'),(21,7,'craft\\elements\\Entry',1,0,'2018-08-18 22:32:47','2018-08-21 08:15:58','d12fb425-d8e2-45a2-806a-0334568b62bb'),(22,6,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-18 22:32:47','2018-08-21 08:15:58','6e02fd77-b4f4-4423-bb15-0fca4da1d6ef'),(23,7,'craft\\elements\\Entry',1,0,'2018-08-18 22:32:57','2018-08-19 09:12:55','83124a72-399d-4859-8ca1-175210039740'),(24,6,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-18 22:32:57','2018-08-19 09:12:55','4dec9dc0-5fe4-42f5-8a10-3c027b993604'),(25,7,'craft\\elements\\Entry',1,0,'2018-08-18 22:33:35','2018-08-19 09:12:55','6a84c7de-b7d9-4d26-a549-3c969ef19041'),(26,6,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-18 22:33:35','2018-08-19 09:12:55','6fd57a44-a1c2-4a20-89f0-720b93bf8190'),(27,7,'craft\\elements\\Entry',1,0,'2018-08-18 22:33:57','2018-08-19 09:12:55','a4f59510-035e-4f11-847d-e6f06e312aab'),(28,6,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-18 22:33:57','2018-08-19 09:12:55','83216a31-0718-4b2c-86de-125638f5ff02'),(29,7,'craft\\elements\\Entry',1,0,'2018-08-18 22:34:21','2018-08-19 09:12:55','8bf88740-ff54-4e85-b723-f66b9c6659cb'),(30,6,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-18 22:34:21','2018-08-19 09:12:55','7607b694-66c6-49e3-93a2-e79a0ee116ae'),(31,7,'craft\\elements\\Entry',1,0,'2018-08-18 22:34:53','2018-08-19 09:12:55','77e4690d-4215-416d-a4f0-ef41dbad1798'),(32,6,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-18 22:34:53','2018-08-19 09:12:55','56962294-334f-4bd9-a4c0-8c53c74afe15'),(33,7,'craft\\elements\\Entry',1,0,'2018-08-18 22:35:10','2018-08-19 09:12:55','d6d81ac3-b87a-4f7e-adc2-7149e1b1bae5'),(34,6,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-18 22:35:10','2018-08-19 09:12:55','26f06856-1964-46f9-af52-ef406eac8ad2'),(35,7,'craft\\elements\\Entry',1,0,'2018-08-18 22:35:44','2018-08-19 09:12:55','16d15d41-5ebe-487d-bb91-ccd371dcb897'),(36,6,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-18 22:35:44','2018-08-19 09:12:55','2e9b48eb-558e-4568-a7cb-60c44c19b39d'),(37,11,'craft\\elements\\MatrixBlock',1,0,'2018-08-19 09:13:22','2018-08-21 08:15:58','c9326606-7bbe-4a16-9955-92dfbc98f5f0'),(38,12,'craft\\elements\\MatrixBlock',1,0,'2018-08-19 09:13:22','2018-08-21 08:15:58','5aac7ad7-6a0d-43be-8822-341ecec0cdd8'),(39,9,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-19 09:18:33','2018-08-19 09:18:33','4d9ed74b-b7c0-4fb7-a00b-2386d451ca94'),(40,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2018-08-19 09:18:33','2018-08-19 09:18:33','1b0b0e48-40be-4e75-be29-8f4baed37331'),(41,8,'craft\\elements\\Asset',1,0,'2018-08-21 08:14:51','2018-08-21 08:14:51','83bc75d1-fc4f-4cd8-b52c-93b330d122f0'),(42,8,'craft\\elements\\Asset',1,0,'2018-08-21 08:15:00','2018-08-21 08:15:00','d1e88b3b-8402-4f9a-97bb-7b8c62063bd8'),(43,11,'craft\\elements\\MatrixBlock',1,0,'2018-08-21 08:15:58','2018-08-21 08:15:58','816e4891-6364-41e2-8d90-110db6bad7a2'),(44,11,'craft\\elements\\MatrixBlock',1,0,'2018-08-21 08:15:58','2018-08-21 08:15:58','2da85e84-3273-4124-bc0a-eda237d99482'),(45,12,'craft\\elements\\MatrixBlock',1,0,'2018-08-21 08:15:58','2018-08-21 08:15:58','66366790-e662-45d5-b386-ddce7529d97c'),(46,11,'craft\\elements\\MatrixBlock',1,0,'2018-08-21 08:15:58','2018-08-21 08:15:58','411171c4-3e2b-4745-9f02-8b856f9c8e42'),(47,11,'craft\\elements\\MatrixBlock',1,0,'2018-08-21 08:15:58','2018-08-21 08:15:58','e67c772f-f3e5-4547-acb2-44efe06e43f3');
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements_sites`
--

DROP TABLE IF EXISTS `elements_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elements_sites_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  UNIQUE KEY `elements_sites_uri_siteId_unq_idx` (`uri`,`siteId`),
  KEY `elements_sites_siteId_idx` (`siteId`),
  KEY `elements_sites_slug_siteId_idx` (`slug`,`siteId`),
  KEY `elements_sites_enabled_idx` (`enabled`),
  CONSTRAINT `elements_sites_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elements_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements_sites`
--

LOCK TABLES `elements_sites` WRITE;
/*!40000 ALTER TABLE `elements_sites` DISABLE KEYS */;
INSERT INTO `elements_sites` VALUES (1,1,1,NULL,NULL,1,'2018-08-16 22:24:19','2018-08-16 22:24:19','379376bd-49dc-4786-b087-0ac6c7b589c2'),(2,2,1,'index','__home__',1,'2018-08-16 22:39:09','2018-08-17 06:02:39','cdbff1cf-e931-4299-8424-a366d72059a9'),(3,3,1,'other','other',1,'2018-08-16 22:39:34','2018-08-17 00:48:38','cc6d3f4a-5099-482c-af5c-492a1ea77163'),(4,4,1,'index',NULL,1,'2018-08-16 23:23:48','2018-08-16 23:23:50','93f156d4-16fe-4ad7-ac2b-ca9b595651b5'),(5,5,1,'other',NULL,1,'2018-08-16 23:23:59','2018-09-14 19:28:27','a9f30d94-28f5-4a1f-9cdc-7f74c48bce26'),(6,6,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','2eec3f9c-6651-413a-9168-dd56c191a814'),(7,7,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','d5c827a0-8918-49a9-b9c8-65cb6314ad54'),(8,8,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','ca37964d-54b3-4993-b09e-5939c38ff7c1'),(9,9,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','a238d1d1-2ab0-487f-a21a-4b1df4c9abc3'),(10,10,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','c5c1e2a1-3161-471d-8482-f14f6e8023a7'),(11,11,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','1e82f84e-4713-4656-b07e-f8e63a549613'),(12,12,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','54e63f33-6189-42fc-9226-2429667ab1b6'),(13,13,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','e0dc0650-7c05-4d82-903c-222a569901f4'),(14,14,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','1c117195-50e4-4e78-b1f9-00fecfa16b33'),(15,15,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','d0db9231-74e5-4488-89a0-4e89ab51caff'),(16,16,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','c3f19b8a-2c00-4608-80f5-a36e9ac9c1a6'),(17,17,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','3fd62692-2c43-4ae6-84c3-2db30c17df67'),(18,18,1,NULL,NULL,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','985216fe-3b81-4618-99e7-19fbec22f6f4'),(19,19,1,NULL,NULL,1,'2018-08-18 22:32:08','2018-08-18 22:32:08','30c2d161-6155-4c60-a710-cb6cdc2d5837'),(20,20,1,NULL,NULL,1,'2018-08-18 22:32:12','2018-08-18 22:32:12','1c7d806e-6d9e-4e25-a660-a025e865110c'),(21,21,1,'the-international','work/the-international',1,'2018-08-18 22:32:47','2018-08-21 08:15:58','5178272c-d2ae-433b-a521-d69be4d0256a'),(22,22,1,NULL,NULL,1,'2018-08-18 22:32:47','2018-08-21 08:15:58','89621be2-12e4-410b-b8e7-a3e56395824b'),(23,23,1,'the-conference-company','work/the-conference-company',1,'2018-08-18 22:32:57','2018-08-19 09:12:55','54059c5f-4760-48a0-829f-30027331b5aa'),(24,24,1,NULL,NULL,1,'2018-08-18 22:32:57','2018-08-19 09:12:55','10b63240-8cfd-440a-96ec-504f77c11d43'),(25,25,1,'edition','work/edition',1,'2018-08-18 22:33:35','2018-08-19 09:12:55','903e8e56-2e04-4803-a161-c87e30ec1f87'),(26,26,1,NULL,NULL,1,'2018-08-18 22:33:35','2018-08-19 09:12:55','1a053b97-80ea-4c0a-bd76-de0c3af7a6e2'),(27,27,1,'at-your-request','work/at-your-request',1,'2018-08-18 22:33:57','2018-08-19 09:12:55','8533d0eb-be8d-47c9-b365-510de8588df9'),(28,28,1,NULL,NULL,1,'2018-08-18 22:33:57','2018-08-19 09:12:55','a1319e40-38b8-48da-b49d-b9fce3bea6a2'),(29,29,1,'international-post','work/international-post',1,'2018-08-18 22:34:21','2018-08-19 09:12:55','facc7e3b-4309-4f9a-a7e7-8300a438ea8e'),(30,30,1,NULL,NULL,1,'2018-08-18 22:34:21','2018-08-19 09:12:55','94e5f39f-4d50-4449-858a-5636a12bd684'),(31,31,1,'culprit','work/culprit',1,'2018-08-18 22:34:53','2018-08-19 09:12:55','0c72eeb1-62fa-4488-97dc-98ca385a380a'),(32,32,1,NULL,NULL,1,'2018-08-18 22:34:53','2018-08-19 09:12:55','bde24a93-cc06-4973-b83e-3a25801fae93'),(33,33,1,'batchwell','work/batchwell',1,'2018-08-18 22:35:10','2018-08-19 09:12:55','06878fc5-2358-467a-8987-67294f179fdb'),(34,34,1,NULL,NULL,1,'2018-08-18 22:35:10','2018-08-19 09:12:55','55fbc886-6210-4846-9d62-d22cfefd7349'),(35,35,1,'aalto-paints','work/aalto-paints',1,'2018-08-18 22:35:44','2018-08-19 09:12:55','f9425697-ec94-4e66-a64a-7d407a47ffda'),(36,36,1,NULL,NULL,1,'2018-08-18 22:35:44','2018-08-19 09:12:55','06032999-0614-465f-94ab-1d13f71e12f8'),(37,37,1,NULL,NULL,1,'2018-08-19 09:13:22','2018-08-21 08:15:58','98a73918-d37b-4ca7-a46b-363e6ec870ec'),(38,38,1,NULL,NULL,1,'2018-08-19 09:13:22','2018-08-21 08:15:58','5b0a5f21-edb8-4f59-a2de-fbcb4e6d5897'),(39,39,1,NULL,NULL,1,'2018-08-19 09:18:33','2018-08-19 09:18:33','278bff94-efa4-4781-b788-f42960601e26'),(40,40,1,NULL,NULL,1,'2018-08-19 09:18:33','2018-08-19 09:18:33','fdf9da1e-be13-4d1a-9dc8-3d31040b472a'),(41,41,1,NULL,NULL,1,'2018-08-21 08:14:51','2018-08-21 08:14:51','c8e760b9-8f63-46f4-9219-250465440050'),(42,42,1,NULL,NULL,1,'2018-08-21 08:15:00','2018-08-21 08:15:00','b68350e7-f8a3-4446-bc7a-6d63d9bd3301'),(43,43,1,NULL,NULL,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','31386f3b-5a7f-4c7c-af23-3e44326b45f5'),(44,44,1,NULL,NULL,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','1b4e864a-8358-4dc7-b4ea-73bf52e24e56'),(45,45,1,NULL,NULL,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','f4097b3c-3069-4015-aa1b-b4f4a09507db'),(46,46,1,NULL,NULL,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','446ad940-14ff-41c4-a0b1-c6208ccfb906'),(47,47,1,NULL,NULL,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','878de214-91ed-4451-91eb-c04793dfec5b');
/*!40000 ALTER TABLE `elements_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entries`
--

DROP TABLE IF EXISTS `entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entries_postDate_idx` (`postDate`),
  KEY `entries_expiryDate_idx` (`expiryDate`),
  KEY `entries_authorId_idx` (`authorId`),
  KEY `entries_sectionId_idx` (`sectionId`),
  KEY `entries_typeId_idx` (`typeId`),
  CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entries`
--

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;
INSERT INTO `entries` VALUES (2,1,1,NULL,'2018-08-16 22:39:00',NULL,'2018-08-16 22:39:09','2018-08-17 06:02:39','db6b3030-c4af-4135-929d-e60d87b702f0'),(3,2,2,NULL,'2018-08-16 22:39:00',NULL,'2018-08-16 22:39:34','2018-08-17 00:48:38','b4f5a9c0-3664-4320-a5b7-4bb26b4a22f3'),(4,3,3,1,'2018-08-16 23:23:00',NULL,'2018-08-16 23:23:48','2018-08-16 23:23:48','b29c4d80-a192-4602-b352-21771f9a4a45'),(5,3,3,1,'2018-08-16 23:23:00',NULL,'2018-08-16 23:23:59','2018-09-14 19:28:27','69714496-b743-47f9-b9cb-7a86432cdb20'),(21,4,4,1,'2018-08-18 22:32:00',NULL,'2018-08-18 22:32:47','2018-08-21 08:15:58','1c4fbd89-7d8e-4d35-9fd9-1bf3b914fe09'),(23,4,4,1,'2018-08-18 22:32:00',NULL,'2018-08-18 22:32:57','2018-08-19 09:12:55','013d71bf-b2e1-4196-a468-079a38a8107c'),(25,4,4,1,'2018-08-18 22:32:00',NULL,'2018-08-18 22:33:35','2018-08-19 09:12:55','213ffbc1-8bf7-4888-a050-869b01a52252'),(27,4,4,1,'2018-08-18 22:32:00',NULL,'2018-08-18 22:33:57','2018-08-19 09:12:55','f7b38d26-e61d-4073-9f20-0883a0426420'),(29,4,4,1,'2018-08-18 22:32:00',NULL,'2018-08-18 22:34:21','2018-08-19 09:12:55','2dfee9a3-6d2c-4268-a9d2-a8ed78a4b18f'),(31,4,4,1,'2018-08-18 22:32:00',NULL,'2018-08-18 22:34:53','2018-08-19 09:12:55','b3d4290f-3f7a-404d-983b-7c02eb40b488'),(33,4,4,1,'2018-08-18 22:32:00',NULL,'2018-08-18 22:35:10','2018-08-19 09:12:55','2da1f893-553d-4238-ba94-fa3c620a199f'),(35,4,4,1,'2018-08-18 22:32:00',NULL,'2018-08-18 22:35:44','2018-08-19 09:12:55','d947357d-8f34-4d87-920d-f563b779500c');
/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrydrafts`
--

DROP TABLE IF EXISTS `entrydrafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  `data` mediumtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entrydrafts_sectionId_idx` (`sectionId`),
  KEY `entrydrafts_entryId_siteId_idx` (`entryId`,`siteId`),
  KEY `entrydrafts_siteId_idx` (`siteId`),
  KEY `entrydrafts_creatorId_idx` (`creatorId`),
  CONSTRAINT `entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entrydrafts_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrydrafts`
--

LOCK TABLES `entrydrafts` WRITE;
/*!40000 ALTER TABLE `entrydrafts` DISABLE KEYS */;
/*!40000 ALTER TABLE `entrydrafts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrytypes`
--

DROP TABLE IF EXISTS `entrytypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) DEFAULT 'Title',
  `titleFormat` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `entrytypes_sectionId_idx` (`sectionId`),
  KEY `entrytypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrytypes`
--

LOCK TABLES `entrytypes` WRITE;
/*!40000 ALTER TABLE `entrytypes` DISABLE KEYS */;
INSERT INTO `entrytypes` VALUES (1,1,1,'Index','index',0,NULL,'{section.name|raw}',1,'2018-08-16 22:39:09','2018-08-17 00:56:43','7c382f54-745d-463a-96d0-2b9a6a39f2e4'),(2,2,2,'Other','other',0,NULL,'{section.name|raw}',1,'2018-08-16 22:39:34','2018-08-16 22:41:02','942cc35a-44c9-4fb4-8995-15c0712fc8c1'),(3,3,3,'Navigation','navigation',1,'Title',NULL,1,'2018-08-16 23:22:39','2018-08-16 23:23:32','76463afc-6924-4216-9d50-dd470522f198'),(4,4,7,'Work','work',1,'Title',NULL,1,'2018-08-18 22:25:58','2018-08-19 09:12:54','3d830e48-7b39-4ba7-96fb-841258121854');
/*!40000 ALTER TABLE `entrytypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entryversions`
--

DROP TABLE IF EXISTS `entryversions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `siteId` int(11) NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` text,
  `data` mediumtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entryversions_sectionId_idx` (`sectionId`),
  KEY `entryversions_entryId_siteId_idx` (`entryId`,`siteId`),
  KEY `entryversions_siteId_idx` (`siteId`),
  KEY `entryversions_creatorId_idx` (`creatorId`),
  CONSTRAINT `entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entryversions_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entryversions`
--

LOCK TABLES `entryversions` WRITE;
/*!40000 ALTER TABLE `entryversions` DISABLE KEYS */;
INSERT INTO `entryversions` VALUES (1,4,3,1,1,1,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Index\",\"slug\":\"index\",\"postDate\":1534461780,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"2\":[\"2\"]}}','2018-08-16 23:23:48','2018-08-16 23:23:48','538d3c58-a174-4cba-9c2d-ea776c1e9b29'),(2,5,3,1,1,1,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Other\",\"slug\":\"other\",\"postDate\":1534461780,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"2\":[\"3\"]}}','2018-08-16 23:23:59','2018-08-16 23:23:59','48b880e5-684c-4eea-beba-49f913752396'),(3,5,3,1,1,2,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Other\",\"slug\":\"other\",\"postDate\":1534461780,\"expiryDate\":null,\"enabled\":false,\"newParentId\":null,\"fields\":{\"2\":[\"3\"]}}','2018-08-16 23:31:07','2018-08-16 23:31:07','68846dc1-0150-4854-8d1e-21b33a68890a'),(4,5,3,1,1,3,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Other\",\"slug\":\"other\",\"postDate\":1534461780,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"2\":[\"3\"]}}','2018-08-16 23:31:16','2018-08-16 23:31:16','4f42f3c3-c40a-4179-92da-7f81d094965f'),(5,2,1,1,1,1,'Revision from Aug 16, 2018, 5:24:18 PM','{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Index\",\"slug\":\"index\",\"postDate\":1534459140,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":[]}','2018-08-17 00:24:39','2018-08-17 00:24:39','e63d7d26-a5fe-4326-9c09-da30f913768d'),(6,2,1,1,1,2,'','{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Index\",\"slug\":\"index\",\"postDate\":1534459140,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"5\":\"mail@jeremyevans.co\",\"3\":\"Graphic Designer. \\nFrom Wellington, New Zealand.\\nBased in Oakland, California.\",\"4\":\"+1 (510) 265 9088\"}}','2018-08-17 00:24:40','2018-08-17 00:24:40','52f2a4f5-79e1-48c7-a7eb-820b3b4ce624'),(7,3,2,1,1,1,'Revision from Aug 16, 2018, 3:41:04 PM','{\"typeId\":\"2\",\"authorId\":null,\"title\":\"Other\",\"slug\":\"other\",\"postDate\":1534459140,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":[]}','2018-08-17 00:37:45','2018-08-17 00:37:45','f93952d5-0e96-4450-95c7-d7225f23f41d'),(8,3,2,1,1,2,'','{\"typeId\":\"2\",\"authorId\":null,\"title\":\"Other\",\"slug\":\"other\",\"postDate\":1534459140,\"expiryDate\":null,\"enabled\":false,\"newParentId\":null,\"fields\":[]}','2018-08-17 00:37:45','2018-08-17 00:37:45','5d7f91de-5631-4549-9fa6-7bd6debdcd10'),(9,3,2,1,1,3,'','{\"typeId\":\"2\",\"authorId\":null,\"title\":\"Other\",\"slug\":\"other\",\"postDate\":1534459140,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":[]}','2018-08-17 00:43:31','2018-08-17 00:43:31','4c99b1cb-fc00-4351-b500-cb74f12369c1'),(10,2,1,1,1,3,'','{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Index\",\"slug\":\"index\",\"postDate\":1534459140,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"10\":{\"10\":{\"type\":\"2\",\"fields\":{\"awards\":\"Large Brand Identity\",\"category\":\"Gold (x2)\",\"year\":\"2016\"}},\"11\":{\"type\":\"2\",\"fields\":{\"awards\":\"Business Communication\",\"category\":\"Gold\",\"year\":\"2016\"}},\"12\":{\"type\":\"2\",\"fields\":{\"awards\":\"Colour Award\",\"category\":\"Silver (x1) Bronze (x1)\",\"year\":\"2016\"}},\"13\":{\"type\":\"2\",\"fields\":{\"awards\":\"Design Communication\",\"category\":\"Silver\",\"year\":\"2016\"}},\"14\":{\"type\":\"2\",\"fields\":{\"awards\":\"Design Craft\",\"category\":\"Silver\",\"year\":\"2016\"}},\"15\":{\"type\":\"2\",\"fields\":{\"awards\":\"Small Scale Websites\",\"category\":\"Silver\",\"year\":\"2016\"}},\"16\":{\"type\":\"2\",\"fields\":{\"awards\":\"Environmental Graphics\",\"category\":\"Bronze\",\"year\":\"2016\"}},\"17\":{\"type\":\"2\",\"fields\":{\"awards\":\"Self Promotion\",\"category\":\"Bronze\",\"year\":\"2016\"}},\"18\":{\"type\":\"2\",\"fields\":{\"awards\":\"Small Brand Identity\",\"category\":\"Bronze (x3)\",\"year\":\"2016\"}}},\"5\":\"mail@jeremyevans.co\",\"6\":{\"6\":{\"type\":\"1\",\"fields\":{\"experience\":\"Studio South\",\"position\":\"Design Director\",\"period\":\"09.14 – 07.18\"}},\"7\":{\"type\":\"1\",\"fields\":{\"experience\":\"Kurppa Hosk\",\"position\":\"Freelance Designer\",\"period\":\"09.14 – 07.18\"}},\"8\":{\"type\":\"1\",\"fields\":{\"experience\":\"Tre Kronor Media\",\"position\":\"Graphic Designer\",\"period\":\"09.14 – 07.18\"}},\"9\":{\"type\":\"1\",\"fields\":{\"experience\":\"ATNA\",\"position\":\"Graphic Designer\",\"period\":\"09.14 – 07.18\"}}},\"3\":\"Graphic Designer. \\nFrom Wellington, New Zealand.\\nBased in Oakland, California.\",\"4\":\"+1 (510) 265 9088\"}}','2018-08-17 01:03:14','2018-08-17 01:03:14','de486d93-c2f3-4ec3-b4ec-c0a2161b20ca'),(11,2,1,1,1,4,'','{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Index\",\"slug\":\"index\",\"postDate\":1534459140,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"10\":{\"10\":{\"type\":\"2\",\"fields\":{\"awards\":\"Large Brand Identity\",\"awardsUrl\":null,\"category\":\"Gold (x2)\",\"year\":\"2016\"}},\"11\":{\"type\":\"2\",\"fields\":{\"awards\":\"Business Communication\",\"awardsUrl\":null,\"category\":\"Gold\",\"year\":\"2016\"}},\"12\":{\"type\":\"2\",\"fields\":{\"awards\":\"Colour Award\",\"awardsUrl\":null,\"category\":\"Silver (x1) Bronze (x1)\",\"year\":\"2016\"}},\"13\":{\"type\":\"2\",\"fields\":{\"awards\":\"Design Communication\",\"awardsUrl\":null,\"category\":\"Silver\",\"year\":\"2016\"}},\"14\":{\"type\":\"2\",\"fields\":{\"awards\":\"Design Craft\",\"awardsUrl\":null,\"category\":\"Silver\",\"year\":\"2016\"}},\"15\":{\"type\":\"2\",\"fields\":{\"awards\":\"Small Scale Websites\",\"awardsUrl\":null,\"category\":\"Silver\",\"year\":\"2016\"}},\"16\":{\"type\":\"2\",\"fields\":{\"awards\":\"Environmental Graphics\",\"awardsUrl\":null,\"category\":\"Bronze\",\"year\":\"2016\"}},\"17\":{\"type\":\"2\",\"fields\":{\"awards\":\"Self Promotion\",\"awardsUrl\":null,\"category\":\"Bronze\",\"year\":\"2016\"}},\"18\":{\"type\":\"2\",\"fields\":{\"awards\":\"Small Brand Identity\",\"awardsUrl\":null,\"category\":\"Bronze (x3)\",\"year\":\"2016\"}}},\"5\":\"mail@jeremyevans.co\",\"6\":{\"6\":{\"type\":\"1\",\"fields\":{\"experience\":\"Studio South\",\"experienceUrl\":\"http://studiosouth.co.nz\",\"position\":\"Design Director\",\"period\":\"09.14 – 07.18\"}},\"7\":{\"type\":\"1\",\"fields\":{\"experience\":\"Kurppa Hosk\",\"experienceUrl\":null,\"position\":\"Freelance Designer\",\"period\":\"09.14 – 07.18\"}},\"8\":{\"type\":\"1\",\"fields\":{\"experience\":\"Tre Kronor Media\",\"experienceUrl\":null,\"position\":\"Graphic Designer\",\"period\":\"09.14 – 07.18\"}},\"9\":{\"type\":\"1\",\"fields\":{\"experience\":\"ATNA\",\"experienceUrl\":null,\"position\":\"Graphic Designer\",\"period\":\"09.14 – 07.18\"}}},\"3\":\"Graphic Designer. \\nFrom Wellington, New Zealand.\\nBased in Oakland, California.\",\"4\":\"+1 (510) 265 9088\"}}','2018-08-17 06:02:39','2018-08-17 06:02:39','72608c11-de72-4b43-b909-4fa82f0ef247'),(12,21,4,1,1,1,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The International\",\"slug\":\"the-international\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"22\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"],\"imageAltTitle\":\"The International\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:32:47','2018-08-18 22:32:47','601c9bc2-fc66-4837-8670-2ea6b910617f'),(13,23,4,1,1,1,'Revision from Aug 18, 2018, 3:32:57 PM','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The International\",\"slug\":\"the-international-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"18\":[],\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:32:57','2018-08-18 22:32:57','a8733e09-a4af-4ec6-9d03-b5411a9aff45'),(14,23,4,1,1,2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The International\",\"slug\":\"the-international-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"24\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"],\"imageAltTitle\":\"The International\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:32:57','2018-08-18 22:32:57','0d340309-c594-45e7-8a83-81ac8c373884'),(15,23,4,1,1,3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The Conference Company\",\"slug\":\"the-conference-company\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"24\":{\"type\":\"3\",\"fields\":{\"image\":[\"19\"],\"imageAltTitle\":\"The Conference Company\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:33:28','2018-08-18 22:33:28','4353044a-8b04-4948-8e7d-0cd1046031e2'),(16,25,4,1,1,1,'Revision from Aug 18, 2018, 3:33:35 PM','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The International\",\"slug\":\"the-international-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"18\":[],\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:33:35','2018-08-18 22:33:35','259cb5ea-cb6d-4082-ba7e-4fe273be3e74'),(17,25,4,1,1,2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The International\",\"slug\":\"the-international-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"26\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"],\"imageAltTitle\":\"The International\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:33:35','2018-08-18 22:33:35','4996778b-bb5e-4196-a87c-417835f0d272'),(18,25,4,1,1,3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Edition\",\"slug\":\"edition\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"26\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"],\"imageAltTitle\":\"Edition\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:33:47','2018-08-18 22:33:47','0db1134f-c678-4679-ae42-07424d4dfc2b'),(19,27,4,1,1,1,'Revision from Aug 18, 2018, 3:33:57 PM','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The Conference Company\",\"slug\":\"the-conference-company-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"18\":[],\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:33:57','2018-08-18 22:33:57','5a2861bb-cf13-4a8a-bf34-69d4b5124cfc'),(20,27,4,1,1,2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The Conference Company\",\"slug\":\"the-conference-company-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"28\":{\"type\":\"3\",\"fields\":{\"image\":[\"19\"],\"imageAltTitle\":\"The Conference Company\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:33:57','2018-08-18 22:33:57','71af5ef9-30bd-4a3c-a99d-a1811d4be58e'),(21,27,4,1,1,3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"At Your Request\",\"slug\":\"at-your-request\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"28\":{\"type\":\"3\",\"fields\":{\"image\":[\"19\"],\"imageAltTitle\":\"At Your Request\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:34:15','2018-08-18 22:34:15','1b0611d9-db60-4cbc-a968-068a7b93e0f1'),(22,29,4,1,1,1,'Revision from Aug 18, 2018, 3:34:21 PM','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Edition\",\"slug\":\"edition-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"18\":[],\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:34:21','2018-08-18 22:34:21','7e938fd4-bc65-4075-8cc7-2691cbe68e84'),(23,29,4,1,1,2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Edition\",\"slug\":\"edition-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"30\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"],\"imageAltTitle\":\"Edition\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:34:21','2018-08-18 22:34:21','7545dd95-1a31-4cc7-8767-031592340437'),(24,29,4,1,1,3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"International Post\",\"slug\":\"international-post\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"30\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"],\"imageAltTitle\":\"International Post\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:34:41','2018-08-18 22:34:41','4b6781b2-32b8-4922-9d46-0d81b5c45079'),(25,31,4,1,1,1,'Revision from Aug 18, 2018, 3:34:53 PM','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"At Your Request\",\"slug\":\"at-your-request-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"18\":[],\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:34:53','2018-08-18 22:34:53','81b8049d-245a-4d8f-92da-1db88a4dd813'),(26,31,4,1,1,2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"At Your Request\",\"slug\":\"at-your-request-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"32\":{\"type\":\"3\",\"fields\":{\"image\":[\"19\"],\"imageAltTitle\":\"At Your Request\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:34:53','2018-08-18 22:34:53','8adcafe4-19cc-456c-a1c2-0c69e4c2743d'),(27,31,4,1,1,3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Culprit\",\"slug\":\"culprit\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"32\":{\"type\":\"3\",\"fields\":{\"image\":[\"19\"],\"imageAltTitle\":\"Culprit\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:35:04','2018-08-18 22:35:04','00c884da-8ad3-448c-bb80-5fcdf826d8fe'),(28,33,4,1,1,1,'Revision from Aug 18, 2018, 3:35:10 PM','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"International Post\",\"slug\":\"international-post-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"18\":[],\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:35:10','2018-08-18 22:35:10','64d8232d-0780-44f7-bbed-d1975c766a45'),(29,33,4,1,1,2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"International Post\",\"slug\":\"international-post-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"34\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"],\"imageAltTitle\":\"International Post\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:35:10','2018-08-18 22:35:10','9b57e3d8-931f-4866-a77b-c7ade056945e'),(30,33,4,1,1,3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Batchwell\",\"slug\":\"batchwell\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"34\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"],\"imageAltTitle\":\"Batchwell\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:35:29','2018-08-18 22:35:29','1b4a18a8-a0fb-46a5-bf54-6f7d8070dfa3'),(31,35,4,1,1,1,'Revision from Aug 18, 2018, 3:35:44 PM','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Culprit\",\"slug\":\"culprit-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":\"1\",\"newParentId\":null,\"fields\":{\"18\":[],\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:35:44','2018-08-18 22:35:44','fbae2ebb-60ea-482f-9846-fe2866100940'),(32,35,4,1,1,2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Culprit\",\"slug\":\"culprit-1\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"36\":{\"type\":\"3\",\"fields\":{\"image\":[\"19\"],\"imageAltTitle\":\"Culprit\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:35:44','2018-08-18 22:35:44','e08c573c-635e-4603-a551-be4af9deb6cd'),(33,35,4,1,1,3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Aalto Paints\",\"slug\":\"aalto-paints\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"36\":{\"type\":\"3\",\"fields\":{\"image\":[\"19\"],\"imageAltTitle\":\"Aalto Paints\"}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-18 22:36:02','2018-08-18 22:36:02','f9db44d6-e105-43ef-bacc-bedff5a8d23b'),(34,21,4,1,1,2,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The International\",\"slug\":\"the-international\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"22\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"],\"imageAltTitle\":\"The International\"}}},\"21\":{\"37\":{\"type\":\"doubleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"leftImage\":[],\"rightImage\":[]}},\"38\":{\"type\":\"singleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"image\":[\"20\"]}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-19 09:13:22','2018-08-19 09:13:22','d989bb8d-db19-4686-89af-75b7ccaa79e4'),(35,21,4,1,1,3,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The International\",\"slug\":\"the-international\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"18\":{\"22\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"],\"imageAltTitle\":\"The International\"}}},\"21\":{\"37\":{\"type\":\"doubleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"leftImage\":{\"39\":{\"type\":\"4\",\"fields\":{\"image\":[\"20\"]}}},\"rightImage\":{\"40\":{\"type\":\"5\",\"fields\":{\"image\":[\"19\"]}}}}},\"38\":{\"type\":\"singleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"image\":[\"20\"]}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-19 09:18:34','2018-08-19 09:18:34','3aa37fe0-72ab-46e5-a914-7cfda6031937'),(36,21,4,1,1,4,'','{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"The International\",\"slug\":\"the-international\",\"postDate\":1534631520,\"expiryDate\":null,\"enabled\":true,\"newParentId\":null,\"fields\":{\"21\":{\"37\":{\"type\":\"doubleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"leftImage\":[\"19\"],\"rightImage\":[\"20\"],\"display\":\"leftBottom4RightTop6\"}},\"38\":{\"type\":\"singleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"image\":[\"20\"],\"display\":\"left9\"}},\"43\":{\"type\":\"doubleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"leftImage\":[\"19\"],\"rightImage\":[\"20\"],\"display\":\"left5Right5Right\"}},\"44\":{\"type\":\"doubleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"leftImage\":[\"41\"],\"rightImage\":[\"42\"],\"display\":\"letTop4RightTop5\"}},\"45\":{\"type\":\"singleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"image\":[\"20\"],\"display\":\"right9\"}},\"46\":{\"type\":\"doubleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"leftImage\":[\"19\"],\"rightImage\":[\"20\"],\"display\":\"left4Right4Left\"}},\"47\":{\"type\":\"doubleImage\",\"enabled\":true,\"collapsed\":false,\"fields\":{\"leftImage\":[\"20\"],\"rightImage\":[\"42\"],\"display\":\"leftTop6RightBottom3\"}}},\"18\":{\"22\":{\"type\":\"3\",\"fields\":{\"image\":[\"20\"]}}},\"17\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porttitor eget dolor morbi non arcu risus quis varius quam. Hendrerit dolor magna eget est. Nunc sed velit dignissim sodales ut eu sem integer vitae. \\n\\nCondimentum vitae sapien pellentesque habitant. Nibh sit amet commodo nulla. Pretium fusce id velit ut tortor pretium. Arcu felis bibendum ut tristique et egestas quis ipsum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Pellentesque habitant morbi tristique senectus. Tincidunt vitae semper quis lectus nulla at volutpat diam ut.\",\"16\":\"2016\"}}','2018-08-21 08:15:58','2018-08-21 08:15:58','9acdb342-43ea-455a-a92f-d7988dc5878d');
/*!40000 ALTER TABLE `entryversions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldgroups`
--

DROP TABLE IF EXISTS `fieldgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldgroups`
--

LOCK TABLES `fieldgroups` WRITE;
/*!40000 ALTER TABLE `fieldgroups` DISABLE KEYS */;
INSERT INTO `fieldgroups` VALUES (1,'Common','2018-08-16 22:24:19','2018-08-16 22:24:19','4f1b045d-e7a2-4a07-a347-49e6538490d9'),(2,'Other','2018-08-16 22:40:05','2018-08-16 22:40:05','11865720-2e8e-40f9-91d0-576bdf809bee'),(3,'Work','2018-08-18 22:22:50','2018-08-18 22:22:50','8e1230f2-7fee-4431-afa7-7f60e0e079d9');
/*!40000 ALTER TABLE `fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayoutfields`
--

DROP TABLE IF EXISTS `fieldlayoutfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayoutfields_tabId_idx` (`tabId`),
  KEY `fieldlayoutfields_fieldId_idx` (`fieldId`),
  CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayoutfields`
--

LOCK TABLES `fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `fieldlayoutfields` DISABLE KEYS */;
INSERT INTO `fieldlayoutfields` VALUES (2,3,2,2,0,1,'2018-08-16 23:23:32','2018-08-16 23:23:32','d1332c76-0e86-4746-b01e-b0b5e05ca860'),(15,1,7,3,1,1,'2018-08-17 00:56:43','2018-08-17 00:56:43','172ff239-6fd1-4af0-8c2f-029d9fc438c7'),(16,1,7,4,1,2,'2018-08-17 00:56:43','2018-08-17 00:56:43','0b6b555d-d7b4-4a71-9f63-c12aad86885a'),(17,1,7,5,1,3,'2018-08-17 00:56:43','2018-08-17 00:56:43','3dcfc90f-b5ee-40f5-be5b-f3614a34ad1c'),(18,1,7,6,1,4,'2018-08-17 00:56:43','2018-08-17 00:56:43','a99287c8-29a9-4c42-a663-cc76348b4f40'),(19,1,7,10,1,5,'2018-08-17 00:56:43','2018-08-17 00:56:43','b81dc496-633e-4313-b24c-71c3939188c6'),(23,4,9,7,1,1,'2018-08-17 06:01:15','2018-08-17 06:01:15','3795a5de-bbba-4747-8eb6-70999a82e1ab'),(24,4,9,14,0,2,'2018-08-17 06:01:15','2018-08-17 06:01:15','b077a39a-9d22-442a-96fd-eb95d1c3cfcc'),(25,4,9,8,1,3,'2018-08-17 06:01:15','2018-08-17 06:01:15','1b2fc803-d088-44b2-9c3a-86f5cd0b385c'),(26,4,9,9,1,4,'2018-08-17 06:01:15','2018-08-17 06:01:15','a15344eb-36a1-4c5b-8295-0364c5fdf5cb'),(27,5,10,11,1,1,'2018-08-17 06:01:28','2018-08-17 06:01:28','f692198d-63ef-4187-8f9f-19f90c182f79'),(28,5,10,15,0,2,'2018-08-17 06:01:28','2018-08-17 06:01:28','215bf2a5-18f4-4002-b826-34802ff93955'),(29,5,10,12,1,3,'2018-08-17 06:01:28','2018-08-17 06:01:28','d7dd8d7a-b994-4193-9895-753ac40fec3c'),(30,5,10,13,1,4,'2018-08-17 06:01:28','2018-08-17 06:01:28','d04c4a35-23b6-4e28-b497-7bb4d42e989d'),(43,7,18,17,1,1,'2018-08-19 09:12:54','2018-08-19 09:12:54','54d1721e-c919-4e67-b797-50703f8c467e'),(44,7,18,16,1,2,'2018-08-19 09:12:54','2018-08-19 09:12:54','71c89d79-4cf2-4ae2-8557-4f484b5a5431'),(45,7,18,18,1,3,'2018-08-19 09:12:54','2018-08-19 09:12:54','03bdeecc-0822-4a02-a624-a66fce6e6527'),(46,7,18,21,1,4,'2018-08-19 09:12:54','2018-08-19 09:12:54','8870a256-c3c7-4a5e-9c0e-0a398e137c28'),(47,9,19,23,1,1,'2018-08-19 09:18:19','2018-08-19 09:18:19','fcd1bb3b-f3cd-469c-89ec-5c8adc01a997'),(48,10,20,25,1,1,'2018-08-19 09:18:19','2018-08-19 09:18:19','6d59c0bd-0aab-4768-b99e-61a17a775948'),(57,11,26,22,1,1,'2018-08-21 08:12:10','2018-08-21 08:12:10','2050868c-dc5b-4d03-b374-0402b788e0d4'),(58,11,26,24,1,2,'2018-08-21 08:12:10','2018-08-21 08:12:10','2186ca81-22e6-46f6-8fc0-1fed8d3997b8'),(59,11,26,27,0,3,'2018-08-21 08:12:10','2018-08-21 08:12:10','8fa1159d-7010-43a5-9d0e-93b005e1080e'),(60,12,27,26,1,1,'2018-08-21 08:12:10','2018-08-21 08:12:10','ba0cf6d8-0898-4805-9aa7-3d08f07dac7c'),(61,12,27,28,0,2,'2018-08-21 08:12:10','2018-08-21 08:12:10','1d6ece38-87bb-41cc-b3e1-cc05062105a2'),(63,6,29,19,1,1,'2018-08-21 08:22:19','2018-08-21 08:22:19','41dfdabb-b06e-4c88-8689-65dfa433a348');
/*!40000 ALTER TABLE `fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayouts`
--

DROP TABLE IF EXISTS `fieldlayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayouts`
--

LOCK TABLES `fieldlayouts` WRITE;
/*!40000 ALTER TABLE `fieldlayouts` DISABLE KEYS */;
INSERT INTO `fieldlayouts` VALUES (1,'craft\\elements\\Entry','2018-08-16 22:39:09','2018-08-17 00:56:43','f3a7a7d1-21f5-4035-8050-091224822260'),(2,'craft\\elements\\Entry','2018-08-16 22:39:34','2018-08-16 22:41:02','74901867-ecab-42b4-8d3e-f1275c185a9a'),(3,'craft\\elements\\Entry','2018-08-16 23:22:39','2018-08-16 23:23:32','3ae3ffd1-77f6-4cfb-8776-0acbfdcb927d'),(4,'verbb\\supertable\\elements\\SuperTableBlockElement','2018-08-17 00:55:13','2018-08-17 06:01:15','f95ba049-4156-4f6f-bab6-dedc6766bb90'),(5,'verbb\\supertable\\elements\\SuperTableBlockElement','2018-08-17 00:56:16','2018-08-17 06:01:28','01e5a33a-20ee-4326-9e58-b68573d2f7d3'),(6,'verbb\\supertable\\elements\\SuperTableBlockElement','2018-08-18 22:25:09','2018-08-21 08:22:19','7bc72c08-9180-4b8a-a8c0-29e2f50f613a'),(7,'craft\\elements\\Entry','2018-08-18 22:25:58','2018-08-19 09:12:54','1148dd74-ff8a-4e20-95fa-f5a1ac521743'),(8,'craft\\elements\\Asset','2018-08-18 22:27:12','2018-08-18 22:27:12','fbf17cb2-f6d7-4725-a78e-65685ef193cf'),(9,'verbb\\supertable\\elements\\SuperTableBlockElement','2018-08-19 09:12:36','2018-08-19 09:18:19','98df722c-1d03-44c8-91b7-155ad2299203'),(10,'verbb\\supertable\\elements\\SuperTableBlockElement','2018-08-19 09:12:36','2018-08-19 09:18:19','344d4b53-ccdd-4c04-855f-56655cb40177'),(11,'craft\\elements\\MatrixBlock','2018-08-19 09:12:36','2018-08-21 08:12:10','ac1f0d55-1ba8-482a-8eb0-52b7a9f3dab1'),(12,'craft\\elements\\MatrixBlock','2018-08-19 09:12:36','2018-08-21 08:12:10','e4af2e36-4080-492a-a64a-c9d47c390a72');
/*!40000 ALTER TABLE `fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayouttabs`
--

DROP TABLE IF EXISTS `fieldlayouttabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayouttabs_layoutId_idx` (`layoutId`),
  CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayouttabs`
--

LOCK TABLES `fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `fieldlayouttabs` DISABLE KEYS */;
INSERT INTO `fieldlayouttabs` VALUES (1,2,'Content',1,'2018-08-16 22:41:02','2018-08-16 22:41:02','2780979f-094d-4dfe-a657-e2a39b5ac755'),(2,3,'Navigation Item',1,'2018-08-16 23:23:32','2018-08-16 23:23:32','7d28cdcd-d9dd-4d69-ac7a-42d772e6cb56'),(7,1,'Content',1,'2018-08-17 00:56:43','2018-08-17 00:56:43','a3f09ac2-075a-4e78-8cab-27e7caa721cd'),(9,4,'Content',1,'2018-08-17 06:01:15','2018-08-17 06:01:15','13073762-35cc-44d0-be9d-82c079ee9466'),(10,5,'Content',1,'2018-08-17 06:01:28','2018-08-17 06:01:28','cc75b821-2f64-4525-b544-5db0c75609b6'),(18,7,'Content',1,'2018-08-19 09:12:54','2018-08-19 09:12:54','72351649-660c-41ff-8cda-2f148cbde518'),(19,9,'Content',1,'2018-08-19 09:18:19','2018-08-19 09:18:19','8de1c88c-9f92-47e4-9e22-124599146dff'),(20,10,'Content',1,'2018-08-19 09:18:19','2018-08-19 09:18:19','af11f2b1-03fe-48e7-aeda-36f2a5248632'),(26,11,'Content',1,'2018-08-21 08:12:10','2018-08-21 08:12:10','ce0bfc16-b5f7-40c5-8bd2-e9353878cd20'),(27,12,'Content',1,'2018-08-21 08:12:10','2018-08-21 08:12:10','41242c55-37fd-44ab-891e-64ec93f1d71e'),(29,6,'Content',1,'2018-08-21 08:22:19','2018-08-21 08:22:19','7cc16985-f794-4c6a-baea-2c3b93eb7424');
/*!40000 ALTER TABLE `fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields`
--

DROP TABLE IF EXISTS `fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(64) NOT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'global',
  `instructions` text,
  `translationMethod` varchar(255) NOT NULL DEFAULT 'none',
  `translationKeyFormat` text,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `fields_groupId_idx` (`groupId`),
  KEY `fields_context_idx` (`context`),
  CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields`
--

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;
INSERT INTO `fields` VALUES (2,1,'Navigation','navigation','global','','site',NULL,'craft\\fields\\Entries','{\"sources\":[\"singles\"],\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-08-16 23:23:10','2018-08-16 23:24:11','193f3196-b14e-48ad-9fe9-0c777c77e151'),(3,1,'Introduction','introduction','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"1\",\"initialRows\":\"3\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 00:23:11','2018-08-17 00:23:11','9673ee4b-0200-4e27-b57b-6cd57356c0c1'),(4,1,'Telephone','telephone','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 00:23:31','2018-08-17 00:23:31','152691f4-a0ed-4c69-8b2d-a4cb3905d884'),(5,1,'Email','email','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 00:23:41','2018-08-17 00:23:41','b8176442-3b19-4b14-a955-217f7a47eaed'),(6,1,'Experience','experience','global','','site',NULL,'verbb\\supertable\\fields\\SuperTableField','{\"minRows\":\"1\",\"maxRows\":\"\",\"localizeBlocks\":false,\"staticField\":\"\",\"columns\":null,\"fieldLayout\":\"table\",\"selectionLabel\":\"\"}','2018-08-17 00:55:12','2018-08-17 06:01:15','9dbd614c-c9d6-4e6d-939c-8d5549118e93'),(7,NULL,'Experience','experience','superTableBlockType:1','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 00:55:12','2018-08-17 06:01:15','535449a5-6f7e-47ab-ae05-aa0651a2c921'),(8,NULL,'Position','position','superTableBlockType:1','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 00:55:12','2018-08-17 06:01:15','0d72ee14-b88d-4e33-9e16-a0048ec20753'),(9,NULL,'Period','period','superTableBlockType:1','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 00:55:13','2018-08-17 06:01:15','0256eba9-7e54-4104-b046-3effee258401'),(10,1,'Awards','awards','global','','site',NULL,'verbb\\supertable\\fields\\SuperTableField','{\"minRows\":\"1\",\"maxRows\":\"\",\"localizeBlocks\":false,\"staticField\":\"\",\"columns\":null,\"fieldLayout\":\"table\",\"selectionLabel\":\"\"}','2018-08-17 00:56:15','2018-08-17 06:01:27','5169c8de-e63a-4a21-843e-6ee6111afdd6'),(11,NULL,'Awards','awards','superTableBlockType:2','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 00:56:16','2018-08-17 06:01:27','aec28c02-fa76-4efb-b35a-cb0c510f80ce'),(12,NULL,'Category','category','superTableBlockType:2','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 00:56:16','2018-08-17 06:01:28','a387affc-a79a-4de8-a857-0d16cb8b575e'),(13,NULL,'Year','year','superTableBlockType:2','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 00:56:16','2018-08-17 06:01:28','a86c9fa0-adf1-41c3-ae81-f40d469757b0'),(14,NULL,'Experience URL','experienceUrl','superTableBlockType:1','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 06:01:15','2018-08-17 06:01:15','58cb30a5-587e-48e9-80a3-60def2f6b4a0'),(15,NULL,'Awards URL','awardsUrl','superTableBlockType:2','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-17 06:01:28','2018-08-17 06:01:28','66e7b5f9-9a18-45ef-aed2-7b34136d728e'),(16,3,'Year','year','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"4\",\"columnType\":\"text\"}','2018-08-18 22:23:02','2018-08-18 22:23:02','b1611579-fc48-4e37-8fe7-999bfcb4ee40'),(17,3,'Project Summary','projectSummary','global','','none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"1\",\"initialRows\":\"6\",\"charLimit\":\"\",\"columnType\":\"text\"}','2018-08-18 22:23:17','2018-08-18 22:23:17','0cf1a190-c60b-4f69-aad5-14c38a9732b3'),(18,3,'Hero Image','heroImage','global','','site',NULL,'verbb\\supertable\\fields\\SuperTableField','{\"minRows\":\"\",\"maxRows\":\"1\",\"localizeBlocks\":false,\"staticField\":\"1\",\"columns\":null,\"fieldLayout\":\"row\",\"selectionLabel\":\"\"}','2018-08-18 22:25:09','2018-08-21 08:22:19','fdcecc84-434e-4de5-bcbd-62c5519e0ec8'),(19,NULL,'Image','image','superTableBlockType:3','','site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"1\",\"defaultUploadLocationSource\":\"folder:1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"folder:1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-08-18 22:25:09','2018-08-21 08:22:19','34ae7ac7-a4d9-4980-b6b3-ea7295e9b2f8'),(21,3,'Gallery','gallery','global','','site',NULL,'craft\\fields\\Matrix','{\"minBlocks\":\"1\",\"maxBlocks\":\"\",\"localizeBlocks\":false}','2018-08-19 09:12:36','2018-08-21 08:12:10','f54fc55c-8b96-48d5-aa0a-683852705827'),(22,NULL,'Left Image','leftImage','matrixBlockType:1','','site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"folder:1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"folder:1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-08-19 09:12:36','2018-08-21 08:12:10','93e1cf7c-ea64-4acc-8efb-624ea9be9506'),(23,NULL,'Image','image','superTableBlockType:4','','site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"1\",\"defaultUploadLocationSource\":\"folder:1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"folder:1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-08-19 09:12:36','2018-08-19 09:18:19','9f270cec-8371-4af0-a894-838187829988'),(24,NULL,'Right Image','rightImage','matrixBlockType:1','','site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"folder:1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"folder:1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-08-19 09:12:36','2018-08-21 08:12:10','ffa1af41-4f57-4390-81b5-891765572124'),(25,NULL,'Image','image','superTableBlockType:5','','site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"1\",\"defaultUploadLocationSource\":\"folder:1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"folder:1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-08-19 09:12:36','2018-08-19 09:18:19','f9b9b17f-4788-4e2a-9dd4-165b61674acc'),(26,NULL,'Image','image','matrixBlockType:2','','site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"1\",\"defaultUploadLocationSource\":\"folder:1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"folder:1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2018-08-19 09:12:36','2018-08-21 08:12:10','f0b53749-2394-4a0f-834c-ef8a35c19e6f'),(27,NULL,'Display','display','matrixBlockType:1','','none',NULL,'craft\\fields\\Dropdown','{\"options\":[{\"label\":\"Left bottom 4 - Right top 6\",\"value\":\"leftBottom4RightTop6\",\"default\":\"1\"},{\"label\":\"Left 5 - Right 5 | Right\",\"value\":\"left5Right5Right\",\"default\":\"\"},{\"label\":\"Let top 4 - Right top 5\",\"value\":\"letTop4RightTop5\",\"default\":\"\"},{\"label\":\"Left 4 - Right 4 | Left\",\"value\":\"left4Right4Left\",\"default\":\"\"},{\"label\":\"Left top 6 - Right bottom 3\",\"value\":\"leftTop6RightBottom3\",\"default\":\"\"}]}','2018-08-21 08:10:34','2018-08-21 08:12:10','953d32be-e5f5-40fd-b637-ae2aaabdc04d'),(28,NULL,'Display','display','matrixBlockType:2','','none',NULL,'craft\\fields\\Dropdown','{\"options\":[{\"label\":\"Left 9\",\"value\":\"left9\",\"default\":\"\"},{\"label\":\"Right 9\",\"value\":\"right9\",\"default\":\"\"}]}','2018-08-21 08:12:10','2018-08-21 08:12:10','eb0ef421-3e77-488e-a421-459dd80fff43');
/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `globalsets`
--

DROP TABLE IF EXISTS `globalsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `globalsets_handle_unq_idx` (`handle`),
  KEY `globalsets_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `globalsets`
--

LOCK TABLES `globalsets` WRITE;
/*!40000 ALTER TABLE `globalsets` DISABLE KEYS */;
/*!40000 ALTER TABLE `globalsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) NOT NULL,
  `schemaVersion` varchar(15) NOT NULL,
  `edition` tinyint(3) unsigned NOT NULL,
  `timezone` varchar(30) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `on` tinyint(1) NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `fieldVersion` char(12) NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `info`
--

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;
INSERT INTO `info` VALUES (1,'3.0.21','3.0.91',0,'America/Los_Angeles','Jeremy Evans',1,0,'e1WlsRJqKhXK','2018-08-16 22:24:18','2018-08-22 23:27:53','f835fb47-151f-401d-9d6f-9e7faa2075c8');
/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixblocks`
--

DROP TABLE IF EXISTS `matrixblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `ownerSiteId` int(11) DEFAULT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `matrixblocks_ownerId_idx` (`ownerId`),
  KEY `matrixblocks_fieldId_idx` (`fieldId`),
  KEY `matrixblocks_typeId_idx` (`typeId`),
  KEY `matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `matrixblocks_ownerSiteId_idx` (`ownerSiteId`),
  CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerSiteId_fk` FOREIGN KEY (`ownerSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixblocks`
--

LOCK TABLES `matrixblocks` WRITE;
/*!40000 ALTER TABLE `matrixblocks` DISABLE KEYS */;
INSERT INTO `matrixblocks` VALUES (37,21,NULL,21,1,1,'2018-08-19 09:13:22','2018-08-21 08:15:58','8e2f977c-0b6e-41af-9c4d-e1bfdbba8563'),(38,21,NULL,21,2,2,'2018-08-19 09:13:22','2018-08-21 08:15:58','82c95213-1316-419e-9e6c-cb1c6e80324e'),(43,21,NULL,21,1,3,'2018-08-21 08:15:58','2018-08-21 08:15:58','c9b8fb54-6665-4321-9d91-9b15983496cb'),(44,21,NULL,21,1,4,'2018-08-21 08:15:58','2018-08-21 08:15:58','0a8426c6-5f7c-4b83-92e9-b63125998098'),(45,21,NULL,21,2,5,'2018-08-21 08:15:58','2018-08-21 08:15:58','1f4f3186-ebf9-4571-9ea4-5d3bd9ddb092'),(46,21,NULL,21,1,6,'2018-08-21 08:15:58','2018-08-21 08:15:58','7a6d0ac0-c6ea-412f-af25-a588ef7144c6'),(47,21,NULL,21,1,7,'2018-08-21 08:15:58','2018-08-21 08:15:58','c12f89e0-5af6-4c4a-a075-0b7f0e78ada4');
/*!40000 ALTER TABLE `matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixblocktypes`
--

DROP TABLE IF EXISTS `matrixblocktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `matrixblocktypes_fieldId_idx` (`fieldId`),
  KEY `matrixblocktypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixblocktypes`
--

LOCK TABLES `matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `matrixblocktypes` DISABLE KEYS */;
INSERT INTO `matrixblocktypes` VALUES (1,21,11,'Double Image','doubleImage',1,'2018-08-19 09:12:36','2018-08-21 08:12:10','6fb06687-2cd6-41f2-a29b-8cc02cd4938c'),(2,21,12,'Single Image','singleImage',2,'2018-08-19 09:12:36','2018-08-21 08:12:10','43f9a368-b37b-43b0-bda4-23295a39d3d1');
/*!40000 ALTER TABLE `matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixcontent_gallery`
--

DROP TABLE IF EXISTS `matrixcontent_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixcontent_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_doubleImage_display` varchar(255) DEFAULT NULL,
  `field_singleImage_display` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_gallery_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `matrixcontent_gallery_siteId_fk` (`siteId`),
  CONSTRAINT `matrixcontent_gallery_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_gallery_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixcontent_gallery`
--

LOCK TABLES `matrixcontent_gallery` WRITE;
/*!40000 ALTER TABLE `matrixcontent_gallery` DISABLE KEYS */;
INSERT INTO `matrixcontent_gallery` VALUES (1,37,1,'2018-08-19 09:13:22','2018-08-21 08:15:58','2b8e4a60-bfc6-4f99-a0ba-69213e7237e6','leftBottom4RightTop6',NULL),(2,38,1,'2018-08-19 09:13:22','2018-08-21 08:15:58','3e1fb07d-e6ca-4750-b729-259c806fd635',NULL,'left9'),(3,43,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','adab7044-f962-4951-b755-ac64157412d5','left5Right5Right',NULL),(4,44,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','7b94a00d-7492-4ee8-9f8b-498b1e029dda','letTop4RightTop5',NULL),(5,45,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','113c2f0c-454c-4e22-af29-a7b6fd32e8e3',NULL,'right9'),(6,46,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','ff962b62-735c-4a7d-9011-8418a9fa12f1','left4Right4Left',NULL),(7,47,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','2668790a-0cc3-4817-b870-bf68e8f83497','leftTop6RightBottom3',NULL);
/*!40000 ALTER TABLE `matrixcontent_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `type` enum('app','plugin','content') NOT NULL DEFAULT 'app',
  `name` varchar(255) NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `migrations_pluginId_idx` (`pluginId`),
  KEY `migrations_type_pluginId_idx` (`type`,`pluginId`),
  CONSTRAINT `migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,NULL,'app','Install','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','379a6aed-2062-4818-9a6a-51a4754f4d39'),(2,NULL,'app','m150403_183908_migrations_table_changes','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','93b28646-0f9b-4fd1-8a08-aebfcd491394'),(3,NULL,'app','m150403_184247_plugins_table_changes','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','cde1e359-9ab2-4a2e-9077-42e74c0cfe27'),(4,NULL,'app','m150403_184533_field_version','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','0d141554-26c5-4092-bcf0-aa15736bbaad'),(5,NULL,'app','m150403_184729_type_columns','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','5e73f9fe-2739-4a91-9557-9fc153e85ccc'),(6,NULL,'app','m150403_185142_volumes','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','980475e8-dd67-40fc-8a47-2430d929663c'),(7,NULL,'app','m150428_231346_userpreferences','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','590fb3f3-e7a3-4bdb-ac84-44145959385f'),(8,NULL,'app','m150519_150900_fieldversion_conversion','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','ed5bd058-54aa-465f-8ce8-0052eaa14bec'),(9,NULL,'app','m150617_213829_update_email_settings','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','5bfe4f46-97d1-4057-af47-bf095cf3ec83'),(10,NULL,'app','m150721_124739_templatecachequeries','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','0aa0677e-1eaf-4b54-b622-e55f1062bc98'),(11,NULL,'app','m150724_140822_adjust_quality_settings','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','032919bb-9fde-46bf-9638-4ae16626ffed'),(12,NULL,'app','m150815_133521_last_login_attempt_ip','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','1865b5bb-f301-44ed-a479-7f8b8cc77198'),(13,NULL,'app','m151002_095935_volume_cache_settings','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','c78e23a5-528e-4b9c-9675-64f415140f06'),(14,NULL,'app','m151005_142750_volume_s3_storage_settings','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','6cf91acc-5cbf-4e59-be4d-e044c113637b'),(15,NULL,'app','m151016_133600_delete_asset_thumbnails','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','f065b137-43c9-4597-8221-a4744f7c4bf0'),(16,NULL,'app','m151209_000000_move_logo','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','7fefc21e-fe9e-4023-989f-f69e878d6777'),(17,NULL,'app','m151211_000000_rename_fileId_to_assetId','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','db5b1f0b-1b37-4383-b340-12aae4c802f6'),(18,NULL,'app','m151215_000000_rename_asset_permissions','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','b5b53c87-1b1c-45f9-9ce5-e5bce9895b28'),(19,NULL,'app','m160707_000001_rename_richtext_assetsource_setting','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','cb7784fd-5591-4414-ba43-5cac0d553545'),(20,NULL,'app','m160708_185142_volume_hasUrls_setting','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','1cdff0d7-3792-441e-a907-9377b97cc792'),(21,NULL,'app','m160714_000000_increase_max_asset_filesize','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','c8ac212d-9344-46d0-a330-57ebb9c75610'),(22,NULL,'app','m160727_194637_column_cleanup','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','5d46e003-b635-4ff7-8b67-35ed8ee91b7c'),(23,NULL,'app','m160804_110002_userphotos_to_assets','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','61be9dcd-6eec-4a0e-a68d-a0abaa5368b8'),(24,NULL,'app','m160807_144858_sites','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','e7830284-bfe7-4b32-b051-191e8e0f1a64'),(25,NULL,'app','m160829_000000_pending_user_content_cleanup','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','a58c4d23-c90a-4130-bcf8-a9524fb555c9'),(26,NULL,'app','m160830_000000_asset_index_uri_increase','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','91ee905b-52bf-4ef9-bf67-725a91610edf'),(27,NULL,'app','m160912_230520_require_entry_type_id','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','e3004dd3-36f6-4144-b4de-9ded851f30eb'),(28,NULL,'app','m160913_134730_require_matrix_block_type_id','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','2f6ce2e9-988d-4a9d-8cf2-fb6e0c150e81'),(29,NULL,'app','m160920_174553_matrixblocks_owner_site_id_nullable','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','437140bb-65b6-4246-a695-e1f607753aa1'),(30,NULL,'app','m160920_231045_usergroup_handle_title_unique','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','3f82c79e-71c8-4c49-a668-1dda009cc9b1'),(31,NULL,'app','m160925_113941_route_uri_parts','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','eff72834-f32e-43fd-8707-26340285c87e'),(32,NULL,'app','m161006_205918_schemaVersion_not_null','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','1643e188-41b2-4abf-b60e-540a96923c34'),(33,NULL,'app','m161007_130653_update_email_settings','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','fafc88cc-c62c-4bed-b860-038733dcd2f4'),(34,NULL,'app','m161013_175052_newParentId','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','eb4b9c06-aea6-4d60-83f2-315b62805273'),(35,NULL,'app','m161021_102916_fix_recent_entries_widgets','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','c37fbea3-983a-428a-8d2a-e19ecc5b36e7'),(36,NULL,'app','m161021_182140_rename_get_help_widget','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','df4e2092-b436-4322-9a0f-ce7b8e24587a'),(37,NULL,'app','m161025_000000_fix_char_columns','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','814b0305-7f05-461a-9e52-c9e7f4f0159b'),(38,NULL,'app','m161029_124145_email_message_languages','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','00b895da-8ad9-4c89-91ae-2bc2cc10127f'),(39,NULL,'app','m161108_000000_new_version_format','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','2661d838-7e9f-420d-8584-d1bc9a4e516e'),(40,NULL,'app','m161109_000000_index_shuffle','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','0c3b77d0-8e37-44d5-ba64-ec494ad1cf86'),(41,NULL,'app','m161122_185500_no_craft_app','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','c3742bc0-1496-47d3-a8d2-384132917855'),(42,NULL,'app','m161125_150752_clear_urlmanager_cache','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','b5984514-1307-48cb-87cd-f65f343921c9'),(43,NULL,'app','m161220_000000_volumes_hasurl_notnull','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','8c2f9bd7-44c2-467a-9432-e6d1ff592cfb'),(44,NULL,'app','m170114_161144_udates_permission','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','f2c3de1d-1b57-476c-89f6-be8822016933'),(45,NULL,'app','m170120_000000_schema_cleanup','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','8768739b-daf8-4ee6-9957-bb3802715672'),(46,NULL,'app','m170126_000000_assets_focal_point','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','bb336ed0-2c0a-4667-b154-f06eb29574d9'),(47,NULL,'app','m170206_142126_system_name','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','166e973e-cd00-4a03-bf0e-1c7d2f4e37e6'),(48,NULL,'app','m170217_044740_category_branch_limits','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','dcb5969d-5ce2-4310-a796-f55c4ec8bf5e'),(49,NULL,'app','m170217_120224_asset_indexing_columns','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','9df35c47-4eca-4703-ab3d-1dd9ef439216'),(50,NULL,'app','m170223_224012_plain_text_settings','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','0b589b7e-f9b6-4425-ba53-ab0aa217c8ed'),(51,NULL,'app','m170227_120814_focal_point_percentage','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','2ade75c3-4df4-455b-be3b-97e59c2a1328'),(52,NULL,'app','m170228_171113_system_messages','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','b37b8d2b-0df9-4bfc-bd40-6ae8a23ddea1'),(53,NULL,'app','m170303_140500_asset_field_source_settings','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','532b5781-086d-4153-83c6-de05a0169b18'),(54,NULL,'app','m170306_150500_asset_temporary_uploads','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','71049a1b-c45a-4213-aed9-30f30c828dd5'),(55,NULL,'app','m170414_162429_rich_text_config_setting','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','a9685c08-685d-4782-b615-0ec0ffb983b7'),(56,NULL,'app','m170523_190652_element_field_layout_ids','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','115fa34c-ab10-4203-baa0-4602ef2f3b48'),(57,NULL,'app','m170612_000000_route_index_shuffle','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','034fd75c-88a9-49a3-9bd9-a907e802dc7f'),(58,NULL,'app','m170621_195237_format_plugin_handles','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','06e8474f-a798-4461-8318-71b672c789f0'),(59,NULL,'app','m170630_161028_deprecation_changes','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','d172a19c-8f22-4438-a205-ac89d794fb15'),(60,NULL,'app','m170703_181539_plugins_table_tweaks','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','29841162-bf84-41e2-b830-88452c05eb59'),(61,NULL,'app','m170704_134916_sites_tables','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','b1082c1f-081c-4c90-903d-ba837c291b95'),(62,NULL,'app','m170706_183216_rename_sequences','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','a7049bbd-08f3-4699-bf72-c78d1c266d8f'),(63,NULL,'app','m170707_094758_delete_compiled_traits','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','cc0f95a3-52f8-4467-a48c-b62c9f421584'),(64,NULL,'app','m170731_190138_drop_asset_packagist','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','af047458-997d-4de4-9686-dcc406fdd5c9'),(65,NULL,'app','m170810_201318_create_queue_table','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','4e95dbc8-b1d9-4df9-a6fb-879090e23d9c'),(66,NULL,'app','m170816_133741_delete_compiled_behaviors','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','16fe88f6-4051-4318-bedf-fed67c51dab1'),(67,NULL,'app','m170821_180624_deprecation_line_nullable','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','7c1506ee-b74d-42d4-9982-d6ce47948569'),(68,NULL,'app','m170903_192801_longblob_for_queue_jobs','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','f7c86062-8225-4e2a-b8c7-b5287e870ce0'),(69,NULL,'app','m170914_204621_asset_cache_shuffle','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','ecc49ec0-0113-456e-b88b-349d5a61f5bb'),(70,NULL,'app','m171011_214115_site_groups','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','c0b0daf8-3392-428a-90a6-38ccd35b37fe'),(71,NULL,'app','m171012_151440_primary_site','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','af08c60f-04dc-4ffb-85d8-88f20544543e'),(72,NULL,'app','m171013_142500_transform_interlace','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','68556f7a-a564-48f8-af5c-4111d65f2099'),(73,NULL,'app','m171016_092553_drop_position_select','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','4616a26a-761c-4a0f-a28d-5773996f73a3'),(74,NULL,'app','m171016_221244_less_strict_translation_method','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','5d408399-ba76-4253-b501-61c7219e8316'),(75,NULL,'app','m171107_000000_assign_group_permissions','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','c842a4cb-5851-4659-a089-8aa1095696f4'),(76,NULL,'app','m171117_000001_templatecache_index_tune','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','550b2213-7194-4dfe-a8da-3906f5d6ad56'),(77,NULL,'app','m171126_105927_disabled_plugins','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','8370814e-896a-4874-a4e9-b3a59f19e2da'),(78,NULL,'app','m171130_214407_craftidtokens_table','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','b29e3078-11f3-44bf-af65-2b17fa83840c'),(79,NULL,'app','m171202_004225_update_email_settings','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','3605a104-c96a-4bf9-8372-fff170d818e2'),(80,NULL,'app','m171204_000001_templatecache_index_tune_deux','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','5cb407dd-51e0-42d1-9c3e-e6d7edede56a'),(81,NULL,'app','m171205_130908_remove_craftidtokens_refreshtoken_column','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','5c3859be-46dc-49b8-a298-1dc0befea90f'),(82,NULL,'app','m171218_143135_longtext_query_column','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','7495c9ae-e0d9-4f32-bafb-c4999f48b0ce'),(83,NULL,'app','m171231_055546_environment_variables_to_aliases','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','0c3ed6a4-4046-4040-bb31-6cf275f83a27'),(84,NULL,'app','m180113_153740_drop_users_archived_column','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','236057bc-5983-4151-8411-7fd5d55fad98'),(85,NULL,'app','m180122_213433_propagate_entries_setting','2018-08-16 22:24:20','2018-08-16 22:24:20','2018-08-16 22:24:20','a8c85cd3-b9fe-4223-9d3f-1854aa56a6ad'),(86,NULL,'app','m180124_230459_fix_propagate_entries_values','2018-08-16 22:24:21','2018-08-16 22:24:21','2018-08-16 22:24:21','b7c88ea3-b67f-4631-9d32-9fde64ab79f7'),(87,NULL,'app','m180128_235202_set_tag_slugs','2018-08-16 22:24:21','2018-08-16 22:24:21','2018-08-16 22:24:21','21fb19d1-bc70-4f6c-b349-4361bac280fc'),(88,NULL,'app','m180202_185551_fix_focal_points','2018-08-16 22:24:21','2018-08-16 22:24:21','2018-08-16 22:24:21','1f581afa-c5cc-4596-a731-986d7919128e'),(89,NULL,'app','m180217_172123_tiny_ints','2018-08-16 22:24:21','2018-08-16 22:24:21','2018-08-16 22:24:21','39314244-72c6-455e-9274-0ce32798c523'),(90,NULL,'app','m180321_233505_small_ints','2018-08-16 22:24:21','2018-08-16 22:24:21','2018-08-16 22:24:21','66cab55b-7785-47d3-89f9-8bb72140bc4e'),(91,NULL,'app','m180328_115523_new_license_key_statuses','2018-08-16 22:24:21','2018-08-16 22:24:21','2018-08-16 22:24:21','9113f2e0-de76-480d-bc35-d0e7259f8b53'),(92,NULL,'app','m180404_182320_edition_changes','2018-08-16 22:24:21','2018-08-16 22:24:21','2018-08-16 22:24:21','40c3b934-9681-440f-81cc-e4076e3a2eea'),(93,NULL,'app','m180411_102218_fix_db_routes','2018-08-16 22:24:21','2018-08-16 22:24:21','2018-08-16 22:24:21','a8a82e40-bd0c-43f9-a794-e0e8d3464c81'),(94,NULL,'app','m180416_205628_resourcepaths_table','2018-08-16 22:24:21','2018-08-16 22:24:21','2018-08-16 22:24:21','58bcf3b9-ba26-4694-b167-ec8aae4d3bb9'),(95,NULL,'app','m180418_205713_widget_cleanup','2018-08-16 22:24:21','2018-08-16 22:24:21','2018-08-16 22:24:21','609c50c4-90bc-43bd-95c5-462b1387d985'),(96,1,'plugin','Install','2018-08-17 00:53:06','2018-08-17 00:53:06','2018-08-17 00:53:06','ce027a91-5b44-4329-a05f-717fe46a7597'),(97,1,'plugin','m180210_000000_migrate_content_tables','2018-08-17 00:53:06','2018-08-17 00:53:06','2018-08-17 00:53:06','56904309-073b-44d1-b066-003b825ffbe5'),(98,1,'plugin','m180211_000000_type_columns','2018-08-17 00:53:06','2018-08-17 00:53:06','2018-08-17 00:53:06','9e335eec-befc-4c94-ab06-71389936398f'),(99,1,'plugin','m180219_000000_sites','2018-08-17 00:53:06','2018-08-17 00:53:06','2018-08-17 00:53:06','fda2d41a-f1eb-447f-ae22-700d80201aa7'),(100,1,'plugin','m180220_000000_fix_context','2018-08-17 00:53:06','2018-08-17 00:53:06','2018-08-17 00:53:06','359a97ce-6e19-4a8c-9eae-ddef86329225');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugins`
--

DROP TABLE IF EXISTS `plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handle` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `schemaVersion` varchar(255) NOT NULL,
  `licenseKey` char(24) DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','astray','unknown') NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugins_handle_unq_idx` (`handle`),
  KEY `plugins_enabled_idx` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugins`
--

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;
INSERT INTO `plugins` VALUES (1,'super-table','2.0.8','2.0.4',NULL,'unknown',1,NULL,'2018-08-17 00:53:05','2018-08-17 00:53:05','2018-09-17 19:45:29','94bc6e0b-9c9b-4e7f-bedb-48ae9627f913');
/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job` longblob NOT NULL,
  `description` text,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) unsigned NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text,
  PRIMARY KEY (`id`),
  KEY `queue_fail_timeUpdated_timePushed_idx` (`fail`,`timeUpdated`,`timePushed`),
  KEY `queue_fail_timeUpdated_delay_idx` (`fail`,`timeUpdated`,`delay`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relations`
--

DROP TABLE IF EXISTS `relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relations_fieldId_sourceId_sourceSiteId_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  KEY `relations_sourceId_idx` (`sourceId`),
  KEY `relations_targetId_idx` (`targetId`),
  KEY `relations_sourceSiteId_idx` (`sourceSiteId`),
  CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relations`
--

LOCK TABLES `relations` WRITE;
/*!40000 ALTER TABLE `relations` DISABLE KEYS */;
INSERT INTO `relations` VALUES (1,2,4,NULL,2,1,'2018-08-16 23:23:48','2018-08-16 23:23:48','7199c8c1-322d-4ef5-bd80-e9115c91dacf'),(21,19,24,NULL,19,1,'2018-08-19 09:12:55','2018-08-19 09:12:55','72a721b0-b571-4a3e-a04b-a8dd44b308ea'),(22,19,26,NULL,20,1,'2018-08-19 09:12:55','2018-08-19 09:12:55','7ed05c34-0b9b-4081-8dcc-0076676636f2'),(23,19,28,NULL,19,1,'2018-08-19 09:12:55','2018-08-19 09:12:55','46c6c406-4958-4d6c-adc2-961d1bed72ed'),(24,19,30,NULL,20,1,'2018-08-19 09:12:55','2018-08-19 09:12:55','d2012777-c715-4cb6-9417-5786b8079e79'),(25,19,32,NULL,19,1,'2018-08-19 09:12:55','2018-08-19 09:12:55','cf5cad7f-c507-4741-b0b8-7c1bfa6b6143'),(26,19,34,NULL,20,1,'2018-08-19 09:12:55','2018-08-19 09:12:55','b1d28823-9fea-41d0-a581-d8dd3a7c0a47'),(27,19,36,NULL,19,1,'2018-08-19 09:12:55','2018-08-19 09:12:55','c6e48c0f-cb5c-474c-a498-b35a76fc5c08'),(31,23,39,NULL,20,1,'2018-08-19 09:18:33','2018-08-19 09:18:33','ae31fa56-2dde-4ff3-bdf8-14cb7be31518'),(32,25,40,NULL,19,1,'2018-08-19 09:18:33','2018-08-19 09:18:33','b1f470cb-ee5f-47fb-8860-b50155da7f25'),(34,19,22,NULL,20,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','c27921e6-2361-42ec-8a84-dc76bc0f1506'),(35,22,37,NULL,19,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','5845ffc6-e0ca-4603-b5bd-670d2d051712'),(36,24,37,NULL,20,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','37102801-fed5-47a8-aaf5-c522af3dde67'),(37,26,38,NULL,20,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','6748a410-0d7a-448c-a24b-e5ebbfb662a3'),(38,22,43,NULL,19,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','e5b8bc9d-38e3-4c04-a382-4e3a6d67c2f6'),(39,24,43,NULL,20,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','d8d85e09-6581-45c5-a14d-5d6cbda2023b'),(40,22,44,NULL,41,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','6da5ea12-36aa-4cc0-98c5-9e5c8ae905a4'),(41,24,44,NULL,42,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','ff9faaa2-3851-4f9b-8aad-249f3c8c934c'),(42,26,45,NULL,20,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','4dab0134-cd36-4ec3-808a-c639c4e28996'),(43,22,46,NULL,19,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','f1b9b2a1-dbd4-40da-ad5d-0196f7826c5b'),(44,24,46,NULL,20,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','0558c01e-b69b-4c77-9875-929cd066f9bb'),(45,22,47,NULL,20,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','cc5b945d-b574-4fd1-ba9f-e241bd0eb134'),(46,24,47,NULL,42,1,'2018-08-21 08:15:58','2018-08-21 08:15:58','7ba51b47-a5f2-419a-952c-6d602daf278a'),(49,2,5,NULL,3,1,'2018-09-14 19:28:27','2018-09-14 19:28:27','78a03e48-8885-44bf-b206-ad4a4f47df10');
/*!40000 ALTER TABLE `relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resourcepaths`
--

DROP TABLE IF EXISTS `resourcepaths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resourcepaths` (
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resourcepaths`
--

LOCK TABLES `resourcepaths` WRITE;
/*!40000 ALTER TABLE `resourcepaths` DISABLE KEYS */;
INSERT INTO `resourcepaths` VALUES ('136582ff','@lib/d3'),('164ad882','@app/web/assets/editentry/dist'),('171ee08d','@lib/jquery.payment'),('18e294e5','@lib/prismjs'),('1968fab6','@app/web/assets/matrix/dist'),('19feb954','@app/web/assets/recententries/dist'),('1a649e6c','@lib/jquery-touch-events'),('1ab6b818','@bower/jquery/dist'),('1dc0a2ae','@lib/picturefill'),('1f8bd3f2','@app/web/assets/pluginstore/dist'),('20d89579','@lib/garnishjs'),('2744fb3a','@app/web/assets/updateswidget/dist'),('29d657fb','@app/web/assets/craftsupport/dist'),('3146bf54','@app/web/assets/installer/dist'),('318b34cc','@app/web/assets/fields/dist'),('33e0972b','@app/web/assets/generalsettings/dist'),('352613ed','@lib/element-resize-detector'),('359e4147','@lib/fabric'),('36ec1a82','@app/web/assets/dashboard/dist'),('3711a905','@app/web/assets/updater/dist'),('37700887','@app/web/assets/deprecationerrors/dist'),('3f1e38e6','@lib/jquery-ui'),('43d2cba0','@lib/xregexp'),('4590d4c0','@lib/jquery-ui'),('49be8f2a','@lib/selectize'),('4b39ed9','@app/web/assets/cp/dist'),('4c628108','@app/web/assets/matrixsettings/dist'),('4ea019db','@app/web/assets/edittransform/dist'),('4fa8ffcb','@lib/element-resize-detector'),('528a350','@lib/fileupload'),('55d012f','@app/web/assets/utilities/dist'),('581295c6','@lib'),('6038543e','@bower/jquery/dist'),('631856c0','@app/web/assets/login/dist'),('66c8dc48','@app/web/assets/utilities/dist'),('672643be','@app/web/assets/cp/dist'),('6d900cab','@lib/jquery.payment'),('6dcd83bc','@verbb/supertable/resources/dist'),('6fe2b217','@app/web/assets/tablesettings/dist'),('71b7a343','@lib/velocity'),('74275791','@app/web/assets/plugins/dist'),('7a6b6433','@app/web/assets/recententries/dist'),('7c2d93d5','@lib/timepicker'),('7da8d567','@app/web/assets/feed/dist'),('80e56954','@lib/velocity'),('848dcff2','@app/web/assets/editentry/dist'),('85759d86','@app/web/assets/plugins/dist'),('8bafedc6','@app/web/assets/matrix/dist'),('8d7f59c2','@lib/timepicker'),('8d8ba7','@app/web/assets/login/dist'),('8ef48561','@lib/fileupload'),('916a9e29','@bower/jquery/dist'),('91b8b85d','@lib/jquery-touch-events'),('933eb2d4','@lib/prismjs'),('961c849f','@lib/picturefill'),('979a165f','@app/web/assets/utilities/dist'),('98b9a4ce','@lib/d3'),('9b6382d1','@app/web/assets/dbbackup/dist'),('9cc2c6bc','@lib/jquery.payment'),('9eb07800','@app/web/assets/tablesettings/dist'),('a42b0df2','@app/web/assets/dashboard/dist'),('a5b71ff7','@app/web/assets/deprecationerrors/dist'),('a9405fd1','@lib'),('ab04b348','@lib/garnishjs'),('b28001b7','@lib/xregexp'),('b394f65','@lib/velocity'),('b8ec453d','@lib/selectize'),('bd304b1f','@app/web/assets/matrixsettings/dist'),('be426776','@lib/fabric'),('befa35dc','@lib/element-resize-detector'),('c0d9fedb','@app/web/assets/fields/dist'),('c262a91b','@lib/selectize'),('c4cc8b50','@lib/fabric'),('c622c290','@app/web/assets/deprecationerrors/dist'),('c7bed095','@app/web/assets/dashboard/dist'),('c80eed91','@lib/xregexp'),('ce4cf2f1','@lib/jquery-ui'),('cf5b315c','@verbb/supertable/resources/dist'),('d18a5f6e','@lib/garnishjs'),('d7268f9f','@verbb/supertable/resources/dist'),('e23748e8','@lib/d3'),('e7181295','@app/web/assets/editentry/dist'),('e8ac7343','@app/web/assets/recententries/dist'),('eb36547b','@lib/jquery-touch-events'),('ec9268b9','@lib/picturefill'),('eed919e5','@app/web/assets/pluginstore/dist'),('f173b91b','@app/web/assets/sites/dist'),('f1df41b0','@app/web/assets/login/dist'),('f47a6947','@lib/fileupload'),('f5e154ce','@app/web/assets/cp/dist'),('f7f1b5e4','@lib/timepicker');
/*!40000 ALTER TABLE `resourcepaths` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) DEFAULT NULL,
  `uriParts` varchar(255) NOT NULL,
  `uriPattern` varchar(255) NOT NULL,
  `template` varchar(500) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `routes_uriPattern_idx` (`uriPattern`),
  KEY `routes_siteId_idx` (`siteId`),
  CONSTRAINT `routes_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routes`
--

LOCK TABLES `routes` WRITE;
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `searchindex`
--

DROP TABLE IF EXISTS `searchindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`),
  FULLTEXT KEY `searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searchindex`
--

LOCK TABLES `searchindex` WRITE;
/*!40000 ALTER TABLE `searchindex` DISABLE KEYS */;
INSERT INTO `searchindex` VALUES (1,'username',0,1,' jeremy '),(1,'firstname',0,1,''),(1,'lastname',0,1,''),(1,'fullname',0,1,''),(1,'email',0,1,' hi graphicactivity co nz '),(1,'slug',0,1,''),(2,'slug',0,1,' index '),(2,'title',0,1,' index '),(3,'slug',0,1,' other '),(3,'title',0,1,' other '),(3,'field',1,1,''),(4,'field',2,1,' index '),(4,'slug',0,1,' index '),(4,'title',0,1,' index '),(5,'field',2,1,' other '),(5,'slug',0,1,' other '),(5,'title',0,1,' other '),(2,'field',3,1,' graphic designer from wellington new zealand based in oakland california '),(2,'field',4,1,' 1 510 265 9088 '),(2,'field',5,1,' mail jeremyevans co '),(2,'field',6,1,' studio south http studiosouth co nz 09 14 07 18 design director kurppa hosk 09 14 07 18 freelance designer tre kronor media 09 14 07 18 graphic designer atna 09 14 07 18 graphic designer '),(2,'field',10,1,' large brand identity gold x2 2016 business communication gold 2016 colour award silver x1 bronze x1 2016 design communication silver 2016 design craft silver 2016 small scale websites silver 2016 environmental graphics bronze 2016 self promotion bronze 2016 small brand identity bronze x3 2016 '),(6,'field',7,1,' studio south '),(6,'field',8,1,' design director '),(6,'field',9,1,' 09 14 07 18 '),(6,'slug',0,1,''),(7,'field',7,1,' kurppa hosk '),(7,'field',8,1,' freelance designer '),(7,'field',9,1,' 09 14 07 18 '),(7,'slug',0,1,''),(8,'field',7,1,' tre kronor media '),(8,'field',8,1,' graphic designer '),(8,'field',9,1,' 09 14 07 18 '),(8,'slug',0,1,''),(9,'field',7,1,' atna '),(9,'field',8,1,' graphic designer '),(9,'field',9,1,' 09 14 07 18 '),(9,'slug',0,1,''),(10,'field',11,1,' large brand identity '),(10,'field',12,1,' gold x2 '),(10,'field',13,1,' 2016 '),(10,'slug',0,1,''),(11,'field',11,1,' business communication '),(11,'field',12,1,' gold '),(11,'field',13,1,' 2016 '),(11,'slug',0,1,''),(12,'field',11,1,' colour award '),(12,'field',12,1,' silver x1 bronze x1 '),(12,'field',13,1,' 2016 '),(12,'slug',0,1,''),(13,'field',11,1,' design communication '),(13,'field',12,1,' silver '),(13,'field',13,1,' 2016 '),(13,'slug',0,1,''),(14,'field',11,1,' design craft '),(14,'field',12,1,' silver '),(14,'field',13,1,' 2016 '),(14,'slug',0,1,''),(15,'field',11,1,' small scale websites '),(15,'field',12,1,' silver '),(15,'field',13,1,' 2016 '),(15,'slug',0,1,''),(16,'field',11,1,' environmental graphics '),(16,'field',12,1,' bronze '),(16,'field',13,1,' 2016 '),(16,'slug',0,1,''),(17,'field',11,1,' self promotion '),(17,'field',12,1,' bronze '),(17,'field',13,1,' 2016 '),(17,'slug',0,1,''),(18,'field',11,1,' small brand identity '),(18,'field',12,1,' bronze x3 '),(18,'field',13,1,' 2016 '),(18,'slug',0,1,''),(6,'field',14,1,' http studiosouth co nz '),(7,'field',14,1,''),(8,'field',14,1,''),(9,'field',14,1,''),(10,'field',15,1,''),(11,'field',15,1,''),(12,'field',15,1,''),(13,'field',15,1,''),(14,'field',15,1,''),(15,'field',15,1,''),(16,'field',15,1,''),(17,'field',15,1,''),(18,'field',15,1,''),(19,'filename',0,1,' grey placeholder jpg '),(19,'extension',0,1,' jpg '),(19,'kind',0,1,' image '),(19,'slug',0,1,''),(19,'title',0,1,' grey placeholder '),(20,'filename',0,1,' black placeholder jpg '),(20,'extension',0,1,' jpg '),(20,'kind',0,1,' image '),(20,'slug',0,1,''),(20,'title',0,1,' black placeholder '),(21,'field',17,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua porttitor eget dolor morbi non arcu risus quis varius quam hendrerit dolor magna eget est nunc sed velit dignissim sodales ut eu sem integer vitae condimentum vitae sapien pellentesque habitant nibh sit amet commodo nulla pretium fusce id velit ut tortor pretium arcu felis bibendum ut tristique et egestas quis ipsum sit amet nisl suscipit adipiscing bibendum est ultricies integer quis pellentesque habitant morbi tristique senectus tincidunt vitae semper quis lectus nulla at volutpat diam ut '),(21,'field',16,1,' 2016 '),(21,'field',18,1,' black placeholder '),(22,'field',19,1,' black placeholder '),(22,'field',20,1,' the international '),(22,'slug',0,1,''),(21,'slug',0,1,' the international '),(21,'title',0,1,' the international '),(23,'field',17,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua porttitor eget dolor morbi non arcu risus quis varius quam hendrerit dolor magna eget est nunc sed velit dignissim sodales ut eu sem integer vitae condimentum vitae sapien pellentesque habitant nibh sit amet commodo nulla pretium fusce id velit ut tortor pretium arcu felis bibendum ut tristique et egestas quis ipsum sit amet nisl suscipit adipiscing bibendum est ultricies integer quis pellentesque habitant morbi tristique senectus tincidunt vitae semper quis lectus nulla at volutpat diam ut '),(23,'field',16,1,' 2016 '),(23,'field',18,1,' grey placeholder the conference company '),(23,'slug',0,1,' the conference company '),(23,'title',0,1,' the conference company '),(24,'field',19,1,' grey placeholder '),(24,'field',20,1,' the conference company '),(24,'slug',0,1,''),(25,'field',17,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua porttitor eget dolor morbi non arcu risus quis varius quam hendrerit dolor magna eget est nunc sed velit dignissim sodales ut eu sem integer vitae condimentum vitae sapien pellentesque habitant nibh sit amet commodo nulla pretium fusce id velit ut tortor pretium arcu felis bibendum ut tristique et egestas quis ipsum sit amet nisl suscipit adipiscing bibendum est ultricies integer quis pellentesque habitant morbi tristique senectus tincidunt vitae semper quis lectus nulla at volutpat diam ut '),(25,'field',16,1,' 2016 '),(25,'field',18,1,' black placeholder edition '),(25,'slug',0,1,' edition '),(25,'title',0,1,' edition '),(26,'field',19,1,' black placeholder '),(26,'field',20,1,' edition '),(26,'slug',0,1,''),(27,'field',17,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua porttitor eget dolor morbi non arcu risus quis varius quam hendrerit dolor magna eget est nunc sed velit dignissim sodales ut eu sem integer vitae condimentum vitae sapien pellentesque habitant nibh sit amet commodo nulla pretium fusce id velit ut tortor pretium arcu felis bibendum ut tristique et egestas quis ipsum sit amet nisl suscipit adipiscing bibendum est ultricies integer quis pellentesque habitant morbi tristique senectus tincidunt vitae semper quis lectus nulla at volutpat diam ut '),(27,'field',16,1,' 2016 '),(27,'field',18,1,' grey placeholder at your request '),(27,'slug',0,1,' at your request '),(27,'title',0,1,' at your request '),(28,'field',19,1,' grey placeholder '),(28,'field',20,1,' at your request '),(28,'slug',0,1,''),(29,'field',17,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua porttitor eget dolor morbi non arcu risus quis varius quam hendrerit dolor magna eget est nunc sed velit dignissim sodales ut eu sem integer vitae condimentum vitae sapien pellentesque habitant nibh sit amet commodo nulla pretium fusce id velit ut tortor pretium arcu felis bibendum ut tristique et egestas quis ipsum sit amet nisl suscipit adipiscing bibendum est ultricies integer quis pellentesque habitant morbi tristique senectus tincidunt vitae semper quis lectus nulla at volutpat diam ut '),(29,'field',16,1,' 2016 '),(29,'field',18,1,' black placeholder international post '),(29,'slug',0,1,' international post '),(29,'title',0,1,' international post '),(30,'field',19,1,' black placeholder '),(30,'field',20,1,' international post '),(30,'slug',0,1,''),(31,'field',17,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua porttitor eget dolor morbi non arcu risus quis varius quam hendrerit dolor magna eget est nunc sed velit dignissim sodales ut eu sem integer vitae condimentum vitae sapien pellentesque habitant nibh sit amet commodo nulla pretium fusce id velit ut tortor pretium arcu felis bibendum ut tristique et egestas quis ipsum sit amet nisl suscipit adipiscing bibendum est ultricies integer quis pellentesque habitant morbi tristique senectus tincidunt vitae semper quis lectus nulla at volutpat diam ut '),(31,'field',16,1,' 2016 '),(31,'field',18,1,' grey placeholder culprit '),(31,'slug',0,1,' culprit '),(31,'title',0,1,' culprit '),(32,'field',19,1,' grey placeholder '),(32,'field',20,1,' culprit '),(32,'slug',0,1,''),(33,'field',17,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua porttitor eget dolor morbi non arcu risus quis varius quam hendrerit dolor magna eget est nunc sed velit dignissim sodales ut eu sem integer vitae condimentum vitae sapien pellentesque habitant nibh sit amet commodo nulla pretium fusce id velit ut tortor pretium arcu felis bibendum ut tristique et egestas quis ipsum sit amet nisl suscipit adipiscing bibendum est ultricies integer quis pellentesque habitant morbi tristique senectus tincidunt vitae semper quis lectus nulla at volutpat diam ut '),(33,'field',16,1,' 2016 '),(33,'field',18,1,' black placeholder batchwell '),(33,'slug',0,1,' batchwell '),(33,'title',0,1,' batchwell '),(34,'field',19,1,' black placeholder '),(34,'field',20,1,' batchwell '),(34,'slug',0,1,''),(35,'field',17,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua porttitor eget dolor morbi non arcu risus quis varius quam hendrerit dolor magna eget est nunc sed velit dignissim sodales ut eu sem integer vitae condimentum vitae sapien pellentesque habitant nibh sit amet commodo nulla pretium fusce id velit ut tortor pretium arcu felis bibendum ut tristique et egestas quis ipsum sit amet nisl suscipit adipiscing bibendum est ultricies integer quis pellentesque habitant morbi tristique senectus tincidunt vitae semper quis lectus nulla at volutpat diam ut '),(35,'field',16,1,' 2016 '),(35,'field',18,1,' grey placeholder aalto paints '),(35,'slug',0,1,' aalto paints '),(35,'title',0,1,' aalto paints '),(36,'field',19,1,' grey placeholder '),(36,'field',20,1,' aalto paints '),(36,'slug',0,1,''),(21,'field',21,1,' leftbottom4righttop6 grey placeholder black placeholder left9 black placeholder left5right5right grey placeholder black placeholder lettop4righttop5 black portrait placeholder grey portrait placeholder right9 black placeholder left4right4left grey placeholder black placeholder lefttop6rightbottom3 black placeholder grey portrait placeholder '),(23,'field',21,1,''),(25,'field',21,1,''),(27,'field',21,1,''),(29,'field',21,1,''),(31,'field',21,1,''),(33,'field',21,1,''),(35,'field',21,1,''),(37,'field',22,1,' grey placeholder '),(37,'field',24,1,' black placeholder '),(37,'slug',0,1,''),(38,'field',26,1,' black placeholder '),(38,'slug',0,1,''),(39,'field',23,1,' black placeholder '),(39,'slug',0,1,''),(40,'field',25,1,' grey placeholder '),(40,'slug',0,1,''),(41,'filename',0,1,' black portrait placeholder jpg '),(41,'extension',0,1,' jpg '),(41,'kind',0,1,' image '),(41,'slug',0,1,''),(41,'title',0,1,' black portrait placeholder '),(42,'filename',0,1,' grey portrait placeholder jpg '),(42,'extension',0,1,' jpg '),(42,'kind',0,1,' image '),(42,'slug',0,1,''),(42,'title',0,1,' grey portrait placeholder '),(37,'field',27,1,' leftbottom4righttop6 '),(38,'field',28,1,' left9 '),(43,'field',22,1,' grey placeholder '),(43,'field',24,1,' black placeholder '),(43,'field',27,1,' left5right5right '),(43,'slug',0,1,''),(44,'field',22,1,' black portrait placeholder '),(44,'field',24,1,' grey portrait placeholder '),(44,'field',27,1,' lettop4righttop5 '),(44,'slug',0,1,''),(45,'field',26,1,' black placeholder '),(45,'field',28,1,' right9 '),(45,'slug',0,1,''),(46,'field',22,1,' grey placeholder '),(46,'field',24,1,' black placeholder '),(46,'field',27,1,' left4right4left '),(46,'slug',0,1,''),(47,'field',22,1,' black placeholder '),(47,'field',24,1,' grey portrait placeholder '),(47,'field',27,1,' lefttop6rightbottom3 '),(47,'slug',0,1,'');
/*!40000 ALTER TABLE `searchindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` enum('single','channel','structure') NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagateEntries` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_handle_unq_idx` (`handle`),
  UNIQUE KEY `sections_name_unq_idx` (`name`),
  KEY `sections_structureId_idx` (`structureId`),
  CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,NULL,'Index','index','single',1,1,'2018-08-16 22:39:09','2018-08-16 22:39:09','1e06b3c3-3932-4055-8857-aa161a1b7380'),(2,NULL,'Other','other','single',1,1,'2018-08-16 22:39:34','2018-08-16 22:39:34','ca5fb142-e5e1-4b7e-9c54-f175349a3763'),(3,1,'Navigation','navigation','structure',1,1,'2018-08-16 23:22:39','2018-08-16 23:22:39','5a61b14b-a235-4729-8163-4afe4b13132b'),(4,2,'Work','work','structure',1,1,'2018-08-18 22:25:58','2018-08-18 22:25:58','7337c9df-ddcb-4174-8110-68f26eab2972');
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections_sites`
--

DROP TABLE IF EXISTS `sections_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_sites_sectionId_siteId_unq_idx` (`sectionId`,`siteId`),
  KEY `sections_sites_siteId_idx` (`siteId`),
  CONSTRAINT `sections_sites_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sections_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections_sites`
--

LOCK TABLES `sections_sites` WRITE;
/*!40000 ALTER TABLE `sections_sites` DISABLE KEYS */;
INSERT INTO `sections_sites` VALUES (1,1,1,1,'__home__','pages/index',1,'2018-08-16 22:39:09','2018-08-16 22:39:09','b3d65210-2bf6-4e72-8017-d0230eedffe9'),(2,2,1,1,'other','pages/other',1,'2018-08-16 22:39:34','2018-08-16 22:39:34','09afd825-23df-49ac-ac4c-f0b952420bbd'),(3,3,1,0,NULL,NULL,1,'2018-08-16 23:22:39','2018-08-16 23:22:39','5b37592c-f784-4737-b53a-a7c8917c858f'),(4,4,1,1,'work/{slug}','pages/work',1,'2018-08-18 22:25:58','2018-08-18 22:25:58','d1dc68da-036f-4195-b1ae-8b615d6928cd');
/*!40000 ALTER TABLE `sections_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sessions_uid_idx` (`uid`),
  KEY `sessions_token_idx` (`token`),
  KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `sessions_userId_idx` (`userId`),
  CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,1,'Z5l8LWQjRXBjiWeqS59EHEPOTlxdj3dmEqJGuo0y2J3NTbuylnv5OpolvNzTaRhkRmjbqsmyNxWddezlORMlsX-pFJHvF9B9ommV','2018-08-16 22:25:09','2018-08-17 03:30:53','ea0946e4-4ea0-4078-8184-c8c579624cf5'),(2,1,'JiHAxyVmujpYsFZF_fuCZus6BA7MldFNRWklr00boTYjqbyeiq0aiSMs8ffLWYO4wJCM0EoEFQAiS1ZLXndFjP8YagEHEjMkVGya','2018-08-17 03:33:35','2018-08-17 03:58:51','1de47fef-b128-4a81-a2cd-3ea4d3377e18'),(3,1,'EPQstTGSqldCmzp6MhMvEGvRDdD-2Fo9YrvuONBuoCYFUIWljFZkfoijIV8lNROOMJ3jtn-XBV5_00v97FZUMJop6LfzOeuMWC-6','2018-08-17 04:33:29','2018-08-17 06:18:35','225dc213-b8ff-4cf2-a71b-3da9f91fef9a'),(4,1,'ZNmkgubMqRkaFY9uHTw23D8phdTG-xOPef0bGz5tK8k9mZK18tEdvJm8FFdCDHAiIzRN4ftItS7GQ0m7Ns1WErFlUmXDEjFo7Es4','2018-08-17 04:49:36','2018-08-17 04:49:36','1d1e3360-ca30-4865-8e6e-f7e6df5df5ed'),(5,1,'0FOXWY3YfWVMfuTSa2QO_BMq2noyi-fNpSiKdIhuO4G4Sd9QL0Cqzf5VqNPyAZHeVlpsN-dSvWwm6RgYwEIMka3o1UsmdaoZb-zO','2018-08-17 05:48:36','2018-08-17 06:36:13','d6bccb5e-3336-42d0-b198-d4552a2c5630'),(6,1,'jHFq_TR3dn3dMk1uCk4kqaazdAAI9JiyBfLV0qsoExN8M_W2xcg1NcTjOaZ2Tfi1lkH0w3xNrqFChmijrFCk2EP82ViscgE4Msg1','2018-08-17 07:12:44','2018-08-17 07:12:44','e54a8e3b-cff9-4211-a5cd-18246bdd6af6'),(7,1,'G9tFkjPP6Ge48xh8B-yft6Dao4QxxUx-5rjJI_DxH4m_FibIjHTueIlHWkWt7eFDoErIrWnAd6p5P2FDwOoMM86rRfLJ2nP7c9Ds','2018-08-17 10:45:18','2018-08-17 10:45:18','e693a7d2-7f63-454d-97a3-cb3710b2dee3'),(8,1,'tGmSVjmbzhux-Pr4JFf566mZfhCr6rolbhUtDW42YmtjG9we3P6y_mKM0pwgt6oNSL3eGrwQZTlHuL4z6bLPU2Uad-gsSoCADHUL','2018-08-17 13:01:06','2018-08-17 13:01:06','4d2be09c-e1e1-4829-b778-cbc139acb04e'),(9,1,'SzM8CSkXlEhpKnJTqsr4Bcdjop9eQyRzZVyUfIsCnm1vtA7w8CZDcAH4jbk7uI4b-17V9u2FDn1_qHj9zGvcdcD89tEiTh5JSl96','2018-08-17 13:26:35','2018-08-17 13:52:44','769d3998-4b54-4c58-b0b0-6179d6f79596'),(10,1,'KkuwJ8RrVzqvxVgzEMDzRAaADeObNEyFI5SZl2eveMkUAOUtALBNCecsnyudb9YS-5fKw3Ej8S5tBPJOvKOL8O6FLudgsPoDnAVQ','2018-08-17 15:50:37','2018-08-17 16:36:16','7b8a5706-c836-4d41-ae88-3e0ddcfd82b5'),(11,1,'OULKeQDrmCjhCzYlTSG5SIingsx6XuzKYU297EpFIMdpwRs2mgNdF1BZgmG2CoazHPyEO7GIMvshJc3KP8y5tDbqrV2VceiJJsI0','2018-08-17 16:53:29','2018-08-17 17:33:19','e69e4f15-0df4-4b3f-957b-898b542691eb'),(14,1,'xcbiUyXrjSS1s5jzSCk02CXEiwqiCQSkhzXV8zfWWRT9MOyUh-yKlMrb4dJe-euDeMU7Pg0jdex_z9ua3MA1CyTAxOw_MuZNYkVa','2018-08-18 18:29:13','2018-08-18 18:29:13','4ac07173-2837-4cf8-8f2a-e662a09581eb'),(16,1,'oqdgcRMxdN5xYM_1Lronsx5b1VzyBCtd6RY7WWaq-E0xA8jwOzNnxmmVwODPWpH9JfCOVnKU1yQroN3kuuBANwhm0HK7vU1cDWLd','2018-08-19 02:46:57','2018-08-19 02:49:49','23c99fac-a0ed-400c-8c25-f47ddff9da83'),(17,1,'C-T40wo8FOCizYQiVKyYeiRd8kXewkKW5BLM7iDIzAgqhrPmK-FLJyH3a9nBRrfSGVQawVbgksmoYvRXPcmAwZe1DOewTVAa8RVy','2018-08-19 04:17:38','2018-08-19 04:17:38','5a3b375a-d166-44e3-b241-8d0c6b5391c0'),(18,1,'dbJYIFyOHrUc6gqA4SIJpZf5NYtPfez1EWJg2B3PzshN3NDWLimWjPcu4yLYvX-7tO65UPgTWnkO6De7wOA7IewckTQg0ljIj_Ea','2018-08-19 04:17:38','2018-08-19 04:17:38','16f215e0-edc5-4791-9f58-47d865a55be9'),(20,1,'8YfzBfvIshHN8H1S8akFfefVJCGlEGK1ZwS5tLm6rTQAuoEY8Ea82cGLpMuO_qegp0ddEpAcLlVNMyoapGYmt1Tlmo20WH5K0lTw','2018-08-19 16:19:51','2018-08-19 16:19:51','f87d133f-6b15-4851-974c-b9747634e4f9'),(21,1,'Az2rJc6pxwgek_inr1GVgxTya6RUYAuNcfv_AS0ES8laYa7IsmkeOlBgQ_VzTK4VgbxvRlMHtmFjX6dU9seh34IzwKdyKoA5fBi3','2018-08-19 17:51:29','2018-08-19 17:51:29','8c08866a-3567-4e3c-ab6e-126419edc0c4'),(22,1,'pFa2pwjSmEOaovQxzEJK1oCr-rH7Cu9Cn7hrcuOpn2kCQj59xGvCGxWWoYbpgojhd4DDrKMeiTInBWgcmZVcB5I-DBtUbfG2-JoH','2018-08-19 17:51:29','2018-08-19 17:51:29','041d9dc6-cbb3-43ec-8629-1ea89c5e391e'),(23,1,'FBUKSVHXdTEM8dWSTbnOJC7p6l7Ma6bxxsZl1weSi_gJpOIPdXN9giSZG5Vtx5IyNZjxi2IghXqamWP5AFkzhGxEA8FUqMUrxzj4','2018-08-19 19:07:12','2018-08-19 19:07:12','a06f67e3-2993-4450-9eac-975c138f7f18'),(24,1,'NnLS7JEbnus2HWNPFjZ-1iCBhPdzR7YaqGK6XJzmEdPVonrKH5rMj-UaYcOF96ZWowTgMhUvgSZu6ATDu84lpntXagUsMW40_aYc','2018-08-20 00:03:12','2018-08-20 00:03:13','6a1f38e4-9612-4dd4-ac68-e332003224b6'),(25,1,'KMv8Dc34vwICnn95o-L8iKgE46Lk84_UhPBryhcXUSm8vLZO_jvTI9_3typ7WqptAsMx57lRMiRrZsAaGyqWfuZ3wtv-HlEXm1wM','2018-08-20 01:22:22','2018-08-20 01:25:10','c74178cd-1b20-4363-a7d4-5702c224c3de'),(26,1,'86IqsBsCtW89DyAWyx_XXke21wtGs1nKAI_PPbYbWNmzU5Ymagdm5n3M_3c-iHevPTVaUPE7btZVvt7CitxekBa54FQmMFey1OC7','2018-08-20 05:51:15','2018-08-20 05:51:22','873fba4e-c389-4e47-bdaa-ee326af98634'),(27,1,'xW7crWsWth6bKtAu0hvA_pUtRDWQU4iHFrxWwakw49rwablnIztk4gz5SuJ7WvXvGw6OOIiiD1E5zwTXcOkhJyqDWi6FHIiCNBXe','2018-08-20 14:56:05','2018-08-20 14:56:15','306fdcd1-a69f-4531-b222-28468f162e58'),(28,1,'eJV45M5q-yZTCoRhPzD33sxOGG3P3IRMT0CSyGuCIW2twmJ4Fy7vCuJ-quHxJh98Ok6F0vf1mfJo-Zvxi6Th7z1PKexiVc7IPBjp','2018-08-20 20:36:56','2018-08-20 20:36:56','d0f7d262-8b33-4489-b4aa-b9f91512e239'),(29,1,'JBQaAcSRDqn5_o423uf2AjglbXQ0IUU_zP6QXnL-OQaclHjR0Ac7D-SEfRa4urtdaBMh_XMZcDaqAB05D0jtq9pKmYmLiIRbRRMT','2018-08-20 20:36:56','2018-08-20 20:36:56','c1f7a9b3-09dc-403a-8c64-4e390153e741'),(30,1,'IXG9m3lJPqEHgTiKxilp5C_pMHpUGM-XjJQ5xn0ZOAcrbkG15mc4WIwT6I3B5vQvJXszUCOaOhcfjF3nnhkrIzr4M8t8_P_3pAvt','2018-08-20 21:34:54','2018-08-20 21:34:54','658b5746-03c0-4551-92c7-0beb77b1498c'),(31,1,'aRlfDrq0q65jr7CzoVMh2O8UqNaQWf9poEyPkPep39z89axW5MT2bHedTjA_PnVuJNsEqP5dYUOiM7ZH0RCc0CDSyaL6LgtxvVlL','2018-08-20 21:34:54','2018-08-20 21:35:01','76d4405a-b5e7-43e0-b7d2-6c692c56f196'),(33,1,'wMBxlv7BPbGkqdPwgz_CJ2eRlboz4gNRpFtQWfDatMBxyy13wRrLRGaJzUNKfG3O7p5NyBplIGjpW2MtMv4czj4JJ73vxDMpYwe1','2018-08-21 15:55:20','2018-08-21 15:55:51','1029c618-5efc-4558-9294-213454558108'),(34,1,'Rj19-uTcuzXWLopDD6tmjRPyd8irSuNGY6aUkXSyQ0sfpERP1ZmOPm5AgFHsV-PlrkTeMPLNrXHHjjDuLgSGa-feJIZZW4QM3hYY','2018-08-21 20:38:01','2018-08-21 20:38:10','b9713b87-5e90-4caf-b2c5-281d60b32fa7'),(35,1,'h6_2Ae3iA7axDOc_zcAA8zlXlA8lOgghG2TJV_sjJIxxT1kkurPMajI8ABY27k_eDUPxpmn6mD8vvQjLLcjeeye5fOxF12XZRHzj','2018-08-21 21:09:39','2018-08-21 21:10:01','1ad7cb73-9b53-43db-a3be-4bc10067cbc3'),(36,1,'gXZubDshBC1oyW7DLp5Kxma654YVDPOpQNIDau6zIfJYEgbaesL1Op73gd4oSlTLMAakIPN17HKdGQUlkhdLlEsXUk7cN3F7RyNA','2018-08-21 22:43:46','2018-08-21 22:43:46','b7919354-7fcc-4f9a-87f5-87d434173e24'),(37,1,'f4mFihLiXqqMelVjuTB7nZH6xpjsqgp1DASXhrGwj-BngQ8o8XcL9MiW-jVmsQu2HiseBe8oXhsGus8nGfF-tR3Ui-00UOeGRTdT','2018-08-21 22:43:46','2018-08-21 22:43:56','c65c41c9-4c79-462c-a128-6c1aa22bd1f5'),(38,1,'VNPjlnq5Ve8O3quHA7vRyQ15473QgSlUAL4j7vBGz_cTff5eAVca4F0r0RWzyyy_37BvaVKb0tqmMVY_ug_qaWi5G0QvHnaEBfMr','2018-08-22 00:36:08','2018-08-22 00:39:51','370340a9-4e13-4c22-8599-69e237b10bf8'),(39,1,'BvtWnBhsGkqxCWKCAbQ2m5NV70wqj-7p54Yuh5eMeTTMC_IdtgTaS__B22nlaYJsea4ZInjwHYpkp41z4Tibub8jDBABlLWpQh8k','2018-08-22 01:38:19','2018-08-22 01:38:20','a28c8ea3-15cf-48cd-bc48-33859bb37a67'),(40,1,'A73M2u-4AYuXF9uTYAS3dbkXxhxA5fjOcgL3U1qhbfHnhlQrU-sHZ3Q9eJto8XNN3mYRsH6rN8gYQLg0bHDPhRri1U2gFE36vgh9','2018-08-22 02:15:20','2018-08-22 03:03:29','614fc60f-2a5f-4d59-b72b-5ee6ccc3c5c4'),(42,1,'zSy3DWsc661GpJwXEzRFApzT2o-zIAX7Wcf8kLLLtLXd68CsphwPQ0gTIqbNVO3Mz221WRDG_ocL2NQViG81PEgpNtA80LxFFKsK','2018-08-22 15:58:32','2018-08-22 15:58:32','7be7bbb3-d4f1-4275-8049-f2c508bbde91'),(43,1,'JJIcF_wB_Z_MQnIWYnxrqmXvMihgb1Zb0niUhsBK3zgGEU8XKEEUp3f6m3VMtV-fGR0n1C11l6NRRuwTQ2ZmpPZhCfPcZONm8gJC','2018-08-22 15:58:32','2018-08-22 15:58:37','028748be-9a55-4411-995e-f4caed5a4c99'),(44,1,'TaG0lLyZRlY13lgYOIN1oRpsaXywGOBLsGwcj9SnO6wBun_k6klSTOah6yy8unMp4GNpcnr_0MJJAIb-q1xJ3a7pbNWWEDgEFoz_','2018-08-23 01:36:27','2018-08-23 01:38:13','98a15816-ac85-41a7-9503-fb43626d18c0'),(45,1,'Mvd7TuuOhC7U6USPx7KvPhWK08xPCizWXNnLO5F9PmigJ1T1052kT_EftAhw_rFA_IMeHwVKYec0nnc8jxUfixSDpsaL2wtbNMZV','2018-08-23 03:27:00','2018-08-23 03:54:42','ae580367-8cb9-4670-98a2-9007683e4af4'),(48,1,'JGoUL_NJ4QQE2KddZq_nGHN9kZ3OleSw8QVAxcLDAwwsPE_9LZD4LMFAFWjjYeyAbge7_9kaBHdxVN98a9qmMKtKncFFY52BkHXt','2018-08-23 09:13:31','2018-08-23 10:02:31','f164502c-52e9-42da-8085-540a45504c69'),(49,1,'zcH4HvoF68kV0ba3vdT2N2kBdA8wpS3mDhrxXmQnYKxOCu1vBVh0dywbZV89G_aCzZNT9_N-pABnCEMaOf_q-Q8uLOMFQRZ8WV1T','2018-08-23 16:34:12','2018-08-23 16:34:17','378cc48a-2fcd-4ad9-943e-d7daa45ed0e6'),(50,1,'3T0RhyCY0EyW8-Z9gHs4RBOxlc7ghRLopAQCZNXppouUvxWM8RU7Ef7K4-s8jyoIga_KX3_2Dxw5eRzhL9AGAMxzI7FPjZQRlIO5','2018-08-23 18:58:35','2018-08-23 18:58:35','ded50430-6084-40ea-9947-2dd1226b65c3'),(51,1,'R2L0cco3wqqvONqQXbLbxTsVsl4432I9dmCwMNXu8xT5Vla3ZBPvtvoZzlameX3HR7Z15RB9zUEXdLV7acbuC8Kt1fir0COF7Kpg','2018-08-23 18:58:35','2018-08-23 19:15:21','c7e32a08-d02b-4e00-a65b-691a7a394f59'),(52,1,'6q4JVHl5cytKnKGNBIOfDbTq4WQjJFihoDDjzolMCELNbQuMOFgNbKXuxPcZ4JN1iz0nSX5T_x13kBsLwSH_ARuUPTZ9n_ngFbXk','2018-08-23 20:45:45','2018-08-23 22:10:18','b76d9e6e-1a21-468c-8671-7a352b9b8c54'),(53,1,'G7A6rU0QCxiLNG3lZKWHguM1i5d4p_s33CWt1el1OYjnFVrKasiKXqoDx3i1gbC2GC6EGzqO2tG_dOZdiM1z-5C0k3nUCH2Fo7m_','2018-08-24 17:06:40','2018-08-24 17:06:40','28696d57-f558-44e2-bb68-9427df98f9e7'),(54,1,'ZXTgYbAgKUS-1bIoGgB1sTxj46NuSzV_VuCBYoICzrd1i6kianWiKsKSG8KY5k3IMsF45G2I6B3xSJ0YyukLMwM3W9qD5izlITE6','2018-08-24 17:06:44','2018-08-24 17:10:39','9697f5b7-fc63-4865-b543-f05a6ae671d1'),(55,1,'bQRaNGvX-LT8Oh-i0AjAetMAQDQP2uDq-zI8BiFP5NJvPmIuWNgWnzOJbvbuAYGYxge7xXToCJM_iNnRzclqlsrthLnfbPmV71WZ','2018-08-27 19:54:54','2018-08-27 19:55:16','630c58fa-8c01-472f-9a1c-350442da8246'),(56,1,'5bbuyFWR-hThSQViq1SCroZmr3KXF_1O2RK0zMDxaBj7szGZ4XffRaIx21tPRRr_RYilV7eb36woDmwNPOCcGoua8l6Z6tySH23u','2018-08-27 21:26:59','2018-08-27 21:26:59','a61ef338-6cce-4a5f-a4e4-8d67bf7c657b'),(57,1,'YHcX2fG29jDHyvgZcstd0j-MntgVhCSFnHNF_t1fmMfn8FPuUFOrqWA-BAz8gpeD0vaKSRPpISE8QHDxb6G_qde0fDub1DTMmgxC','2018-08-27 21:26:59','2018-08-27 21:27:03','db5e5d41-26c1-4511-992d-4974b0c13bfe'),(58,1,'kZXAN11w2lgPc7lWxLX0hmz7QG3g27tOOVRh2UyBHc1el_mPtVTyY9vdmfjSGLxkHEASJpiJYZ_TxZmtFi__ue-Mw_6StZwJpDjv','2018-08-27 22:33:54','2018-08-27 22:33:54','b39dbd40-373c-47bb-afc9-2b7518981ac9'),(59,1,'2SHm9S15t9xYFI4EHBqHQAwp6XAZyA22lQBqH-47PxZZ7FbTlvEBXdEYCxDjq2J4-kaXY9tJyuJfYvSpf9WDOcwA2gezvLZcAlUB','2018-08-27 22:33:55','2018-08-27 22:33:55','f3245035-0440-4d8e-a2a9-7d7256731a54'),(60,1,'eohd-LSxbPSir40jYr8HDAR8lB9KWx3ee40CxZmJyJKsJeKEE16E9QgXWoAbwM3o4nN-tMcDwu4WLXh-pvsxkMIqlqYO58BfAwxn','2018-08-28 03:21:56','2018-08-28 03:59:18','2bbbecff-ef36-4880-9831-62b933a81a04'),(61,1,'mYmdJIVjBHugsC7hcUXwcirWdgpxeMJ4wpjYyO-GrTVwq1fMkrQKyJQp2SfPACCtlPBIhOdkL1YJ-EvjWyxNGNn8RKiXYgyIcNR7','2018-08-28 17:46:28','2018-08-28 17:46:28','7d84d97f-a0fe-4482-bef0-5e92b6409e3d'),(62,1,'C7gegzbHXHg37N8-z_BG4BKW3Rmwye2vLiBD_PzN65UJzu3bBJU2E4BFoYZDTm_APsA5QATSwEHhX3g-pKRdkbc8X2QwgKvxWX61','2018-08-28 17:46:28','2018-08-28 17:46:28','860bf59b-2e91-4c5f-813a-90a581c70db3'),(63,1,'9odV8rb1syYi6zJAaQn5KrDORK701-BVeySvVEtSFd9Bjya-j9RIZ5J1r-U6fJ0b5dh6ECLcvbm6jMZsaAtFQ0OsF1B01SKJXoJR','2018-08-28 19:46:35','2018-08-28 19:57:55','b750c714-ff42-4710-b67e-613381ab6fcb'),(64,1,'d65eYp9xOjpHkrdiRyqIB_sm1g0xcuqQ4EPSW--cp8-yJRjaM71a_Hou33w3M4w0r6HreNKNMcqO51LrM3Nk0PxXbhYsqF_lJO0M','2018-08-28 21:03:35','2018-08-28 21:24:24','4a050ab7-04ba-4e75-b3bf-94b827a00a16'),(65,1,'qEUNlEXz5kSDLHo5ie4Q60aPqeEwlyJIa3slbqzH36Ez4PmmLt071E_vX9ircDKOoXSpS4m4NP9yh6_TeZrz_zthcuzXo5_zGaSJ','2018-08-29 17:42:08','2018-08-29 17:50:50','b3af38df-d227-4cdb-8e42-e15c636d6815'),(66,1,'CK6jH9UzX4sbK3OMJosOhLXBkP5q3gTbyzgXAKKyQdr86Y4kY0wwCikEop8PLJTJptpRsigJwktX0jKGxDYd2n0ehqI_KAYKaDse','2018-08-29 18:41:12','2018-08-29 18:41:12','68f9a852-a0fd-4487-af4a-ac19e03af0f5'),(67,1,'HOZoVlXxBkqgmvpaeoxD8qZjs5-TlaRylZ9Dy6_sxuB04aFL7zsf9ICoZHc2De2c5-G7xo6bEJ1YFNGHCIlo03mTWr2Mv1GBsucs','2018-08-29 18:41:12','2018-08-29 19:01:30','a266b58d-6a1d-4498-b971-de799f18adfa'),(68,1,'zjTByMSpfqUdsmaBtCnbLb_WQMztJC5zBrRVdlYmWKYtCpAskxB8IZcmV_u2mfWsWW2JKLWN3Fa2L3U6CAqAc9A90dct1ZoIzoIa','2018-08-29 21:46:49','2018-08-29 21:46:49','dcdff3c8-94fc-4cc9-b12d-4508619a767c'),(69,1,'-m7-kGYR_CChe5xe48r1sR5IaRN97csTe6lghVHXj55l2bDXVyr4vDsUv03RFSQfiiG9SqR5aKVy3aPWa7orsQbWB49oMUV6J-gU','2018-08-30 04:10:30','2018-08-30 04:10:30','30fc2b8e-dace-41cd-81af-d153a0d815dc'),(70,1,'C4yPrm9DCtEEQgePI-Oo6JNb6cxUHt55h2Y7vChnyh7yN2SHUNHVU6H1kSEU4FgQv0IMBFuBHCxEqBpiSPzJPdiRQIfQ5cirfvkq','2018-08-30 15:48:29','2018-08-30 15:48:29','da8d629a-09d1-42fe-900c-0adc7deec5d1'),(71,1,'_yh6NBX5as1i9CE6NFjtW2Ba9H8wvHrsZ2ZTpixtHc_-xXOeL9ghgRl-0jck3JsLOKlfpbHoMZoBBaEx1WdXw7FZc9jVmuV9JCn-','2018-08-30 21:21:40','2018-08-30 21:21:40','33cc7acf-1cdf-4718-a203-cb9227abfa58'),(72,1,'L8auAwhZZiByIJIitxq1a8h-abH1RPbUhRruOLXp3ne4bVHH-SlCBWBrCVGnZD05MWOZwDVu_P26fx2X0rwoQdAnqyQul1vgpFao','2018-08-31 01:09:19','2018-08-31 01:11:36','b5aad469-0b87-463b-bab2-d659b5cc73ff'),(73,1,'Qn0aTpCQV0IBPGfReXjQoluC05DUUDiz7-2L3FEL_mG8Lb-vU9BRAI1zPpOUNoCRWmNFG1nwqeBenn_zyTCSUXfCyROSr_V2xW-F','2018-08-31 03:24:06','2018-08-31 03:29:31','8b07476c-f248-4c76-9a6d-8b1fea2ab75b'),(74,1,'OHzD_N3htnK38UdG_oVSrHLuf1XOKRcUobSZFTcJqrkTJkf042C3h5xRJWESISFwR8qTe6_Auwhe0ccXJJd_s9tDT3j8g94QfwtU','2018-08-31 04:51:00','2018-08-31 04:51:00','283fe961-3455-418c-a577-688430b22b01'),(75,1,'mNzFUIn-2f8Sz765QjPdWLOw1Y04i09LUq1a0KRfMIh6nopNmBJHaQb3LukuscLiRiEBPvzV7wZZxIkqBDs4HFr6rOn9prXKp9kK','2018-08-31 04:51:00','2018-08-31 04:51:00','bca7aa2c-1460-4d8f-b175-34660b251650'),(76,1,'mSUkcgYWUfaK99WrTU-MjV2QeOEu8eiqD2q16NO84UjtAPZsm-FFgXUVETWV92qfWj_WazWWqguI70pceaNpiMhsJ1KDuueJE3AC','2018-08-31 18:24:39','2018-08-31 18:24:39','c7f266cf-9ff1-4fd4-b88a-9ce9e960968e'),(77,1,'MEucKthaV_Bo7dLb0518-I8AlEW-vdNQAz_fAWaYQAnsxdN7OtwQdKSYYbp7II_HljrAowVznEcQYRpo4iN-4ePaQ7PboUuesaHv','2018-08-31 20:36:21','2018-08-31 20:36:21','aef12e46-ab72-4c43-8372-fef3de8af103'),(78,1,'IiacGQCmTOGe3mow90Sbu6_iK2RGXYs5qewuGdThR_8fqHIyElH10B1C1m5UVoPMmmOXn1kiJKuP5Dg1nxpoUcWtWzUdVhtlE8Yd','2018-08-31 20:36:21','2018-08-31 20:36:21','b20adca6-4b68-4faf-834d-1e8521ea8485'),(79,1,'eUS04BBjcGs7aAR_eE2j_bYM-ozks2crrN3bxfY1wguq5L7TwuniMon29vfnCgTKwJs18rQWcXXzX5jZsRIAYID7z_CNgv7Dg6UE','2018-09-01 00:59:17','2018-09-01 00:59:17','2ba7544e-f28f-41ef-90e7-e54af9dd6e89'),(80,1,'0Ij1jLu1zRnOBh1itkhPXHWlCYNg2ZX7Gnt-Wom68qPUpY23qSaUL4koihlmwA_Sf5oZ9N2_Zc_JbDY6oZ6StcfB4t1T0ZT7pjkU','2018-09-01 00:59:17','2018-09-01 01:21:16','b2d2c38e-497b-49ff-ae21-b9d0187a82f3'),(81,1,'gNQdR8OyNdZI0YOoDdsqs1P7CWrnVVlSe7NhceTxR8e6X7h_SVW1KwbSMAWNCGEED6i1ihqZkrjhx5w5wRc-POFuKek_CZrtXrsY','2018-09-01 15:55:48','2018-09-01 15:55:48','1476e6f4-e305-4134-90ab-b0cb660719d4'),(82,1,'O1UuXbA6qJ7Jv1ZfJeO-d6VGx3o6SBf7VLdY7tgvKlqHNGlnuDnTv8jMCMOllEgRt3GWGwISLE1k2-gFe0FZun_iWqc6wJLiTMzQ','2018-09-01 19:30:06','2018-09-01 19:30:06','8a76ead7-e96a-4150-98c9-99e18eb38316'),(83,1,'3ZKiva9mMOmF6OgQRZhn2zyFPWIVuP5KV_5l5JIEHI4tKhEt1yfJVJVWdK-DWAqWFiBAHgozJR33GWGy5P34ligjOBoMl6VP-hZ5','2018-09-02 02:18:44','2018-09-02 02:18:45','a21c7645-0fde-45fc-ab92-8385506c226c'),(84,1,'rhAU3wtf1rpeAAGYWQaDWvlLoIenOnQYQomdT4ZTiwWZ1OIOFLIV4Xpwbb2zuATHCQ_DAB3POIFI3B2gfICYlwJcYFcRvnJ5jdxb','2018-09-02 16:58:43','2018-09-02 16:58:46','1774fffd-acab-4eb4-89c3-6c7bf706d47b'),(85,1,'FAtLbbdTYTZ5sv_5m_cEIAk6Vi5OY4yvR4n847gXwI3g5HVvzAALVqmnNRs-zB9xk2NmB_8w7ovDIVnVq1bozFk_YPl5ytsOShJn','2018-09-02 23:47:26','2018-09-02 23:47:26','0034a48b-f7df-4aeb-b7d2-3dd14a7ac8ca'),(86,1,'aIhBz63BCDQyGbavo43EycE2IabvnC7rJ-lpAkwI4No5B74IKVAXdyz0FYf_mh6LHTJUa8FsBbsHki9c_nvgS7GMh8OIGo2IrVKt','2018-09-02 23:47:26','2018-09-02 23:47:26','9604500b-a967-42cc-aaeb-b85a536ec282'),(87,1,'MPz_C3bgIF3mM1LuFs1k98maYzzxANMgHeLIT9w-CQU7_kMrzjF66vSRv1wZEw5RMLM3X6wStsvivimUIgBGRcdHi1pF63lEBtHz','2018-09-03 04:01:26','2018-09-03 04:01:26','6bdcf5ae-6c9e-42da-8fcd-819567537792'),(88,1,'Lqq_s4J8n7do9GFwebNsBBjp7tWUwZo-XK54s0rDrKTbccxCZ7rhGCNuOBYS50ezutEMMNfX23vG31WmiuhhP1QjfyHW9D6Cxnv5','2018-09-03 15:36:50','2018-09-03 15:36:51','b750abc0-fb14-44c3-a249-ec46864fc43d'),(89,1,'reqrNSgmMnsBaoLqvUZ_ijSO1C-f8kNns4I-Nx_d2tF2SE_ia-W_X3EZ4TsmKiMLIqGtVHCDYHkNzvgOnIovIXCwiQbs_nu9U2T_','2018-09-03 18:58:57','2018-09-03 18:59:04','2175094f-0e2d-4798-afc9-936c921f41bf'),(90,1,'SfleqTrz3Wd7nU6ewRwz-24LWpmVMTMkid3LDun-yAPJ5HKbVWpsApP6R7PmcXXF0D0ILu73GxEY6bGU0xySnmvHCxxcn2Psi_CI','2018-09-04 00:57:57','2018-09-04 00:57:57','5c140c55-b2c0-4062-978e-21adb587d8c5'),(91,1,'Sbg1pG5TsjRuLhzj4dXVU3zHy63vT34cjQHvNq3gFP3-q3B2h8kEPH_vMFrG-k0xcCP7pCASxoX_e6SxeeAnyp4Qb5Zu3DrgwZbU','2018-09-04 00:57:58','2018-09-04 00:57:58','9580ecce-801a-457e-b3e3-7cee58981b18'),(92,1,'i8sqd-avePbkAJOzs7IV0DpJ4uQpo2h6du1Z2bl9ifopQD8eulZAZHp5W6W3JpSLVkkIX3OfM2zZgypbxORENw-t03GSsSBoleoH','2018-09-04 04:13:49','2018-09-04 04:13:49','8d9347e6-771d-4a25-a720-f21c16e5887e'),(93,1,'6o_-wJi4VOffuOEolboiNAHQ9fRVnr_6ILS-YisaP2ZtRt7j49o5OKaJCgzMdgZ-ZeALbJqAL9utgpCYR7Vm-60yl_YVD3FsQCgO','2018-09-04 04:13:49','2018-09-04 04:13:57','165a1308-fc2d-45e8-9abf-a36e69902182'),(94,1,'MbUUSyKLm1yMBMCCQM-sSPfGtPx7qn2ZRYvidjUWIp41tnZXYgrOH0-nHqyFwrsOBKKHDmet4SuXSgeinSRCmgHdi6p-iicTbD-7','2018-09-04 18:18:51','2018-09-04 18:18:56','79e4dad1-a134-45da-8c0f-0363194b7875'),(95,1,'8kg771cjXIwagCJw4kdlCYES-xZvzGTiOzEHdVGq_izFdRoe_xh6nWVHP36pe294VO0Gy12rePSdt9ccZ92N3kTZA0X8TBXrJIpm','2018-09-04 19:43:16','2018-09-04 19:43:17','b24e0269-67ef-4ae8-b0ef-6eb7fb9fa4fd'),(96,1,'tp4x8HqrZ4DacoojMCdmMUf1ONu7gRsQ0mxLJlnPLXB1hKRmjsqe4meVZd83QkQdruIBNPhQJZXAciNsnAFNz8PhCkCb6wd3ywl4','2018-09-04 22:37:52','2018-09-04 22:37:52','3bcb5f0e-215d-440b-8041-eddf9f5865c1'),(97,1,'PuT0qsBw8R_LPu8IxfhSlNENRaioEfv3_cM2TSKAcyLeyAZDkE99sRKF9ylnqmQ_V6JnSpGkZ3isPbB287N4H_pgsGRXayxhdDud','2018-09-05 20:34:02','2018-09-05 20:34:07','159dfdfc-4c39-4333-8b54-2b0c60f48397'),(98,1,'Y06NvBxeSwWZFmuowrvKcszFycwCJ5pj6BQz0TFrgiyMyvSJrcQLI9hG-q9ggiNR_lF4jQFNID-2tOl2ocClYgdNfbC4HsFVplMi','2018-09-06 18:53:08','2018-09-06 18:53:08','c5883c85-21db-476a-98c8-a4e4d4644411'),(100,1,'CamvYYJNfy3eWKlyQnDc_hkfrEoLbBzkedGT4n1jCzzxzMRGrKMRcfbd3qDMo-BlIhOFpVmoXP9PHUs-NfWPK3Y4btGewZI0s9IZ','2018-09-12 05:07:32','2018-09-12 05:21:30','faef1f12-13ee-462e-b2d4-1e370045a290'),(101,1,'yRHITonUGvkpohu67YLFi1NHR_12ol7QUaoi_bVyGk39Q_686j1mXqHVI9QYBgWv7VfWfHsykCUvvHJZVQ-2k0WcKtkv2sj42uGa','2018-09-12 05:07:34','2018-09-12 05:07:34','859b3bb9-6d45-4e4b-9d01-c289447dd59b'),(102,1,'qx7JoAPA8QVy9NZ_7-646YtETHtsVQowH0Ne9sDyhQ8boD8zTbYEG-mQr3z5WnCQov_wy2dbpHS2ZAwF7AIFEjuye2Izq2XO3Q2-','2018-09-13 03:48:40','2018-09-13 03:48:42','a4f10238-a28f-4f4f-b8b9-d177fa9798fb'),(103,1,'6do-5GSgdMQwEsI9gEArEVN2tVyMSCow5_couYHzd5qKi2-QdME0waeKLOKl4ecTEJP9CB6eWMufqpIYtOA1dm93CU-UthTjORSK','2018-09-14 19:16:26','2018-09-14 19:54:15','122454a6-eb84-44e9-a4a3-6deac8687421'),(104,1,'hHSI6pwnGlnBR4fB3JTTvMehqh4ybKtxQjXAawAMjnuUAcex196zaTWxd9FePmW1lcmBzfHvEJ7S-IytcFDCpJ8Spv2EkK0LI9eC','2018-09-14 23:01:54','2018-09-14 23:01:54','cc2f3810-902b-4f80-90dd-d691e026a264'),(105,1,'Xos1l-UcXon5dVzJToayzC17lMPeZL0XpKOC3LBkXZprHN6mY7mL18-axJ4uItMSzcc7ULeBIOLIhWU-7ZGzONsPxKybXSKorpbG','2018-09-14 23:01:55','2018-09-14 23:01:55','b7685b7c-c117-43e0-a244-a9d136c72824'),(106,1,'mqc5jx1F3d-mKzW0Fch1ohpxYjegqz0Q2mbH8hjr3ksDKnTIlxfQbmrvvrEb18oFpO0tgxt_qF8z5_KW1vt0k94liD6ScdER2Abo','2018-09-15 02:57:11','2018-09-15 02:57:11','125d85c5-a188-4f4e-8ce6-8d3e1a6475f2'),(107,1,'dp4QwZBx47kq3Y9sWGTzXwWv2K1tNpAxiGKuk28QiUVrUOhzt41v-O98fMFi0JFIszEh0G9aThlbvxAHKg8HjHorqPsoCa1w1zE1','2018-09-15 02:57:12','2018-09-15 02:57:12','9a7b3954-0dd6-4439-8ade-598fc0e7a866'),(108,1,'sLPTw3HFxxCDDCA7aEkvmjzMf1uVTqMF-II1J9_r0lKjGUrFKgHiwLEPYFjIwChUwrus-FTXjB6cnQ-JfzlM7AUs-d1NN2nlriNH','2018-09-15 15:35:30','2018-09-15 15:35:33','22800a92-fb04-467d-b001-bd8bcad9b5de'),(109,1,'-ZBfLPUtBdUGoercKuouILL7NifoZm_SCv-puE_pcQjCbnY6hY6s5aWjUABJbCvXZMlsanDIpIZIZj4ik-VXPGRsFDUCgB_VJwni','2018-09-15 17:12:09','2018-09-15 17:12:09','853af675-9876-44d4-8f24-8d2b9105088a'),(110,1,'dlRlBEYIIwScykwCA2lWI-PuHnVNIG80yxzIWKMPyxMKIDjS4KNziLuBt-I2Y5XTP_X7JwTUkbgE7Db9_QwSoJEJoNO7TVVX0G3b','2018-09-16 17:58:14','2018-09-16 17:58:24','a069e68f-e813-4c42-a043-36f6f10ff05a'),(111,1,'4MOqFqHRyjva_uhWJ-0StBRg3pR8Y1aCgLuJJxmvViVWkjYNQ4eguukv5GFEm8Z2FBV4Qi_0HXlo9Ufbmv8C5JpeNgO1iS43yijU','2018-09-16 21:19:29','2018-09-16 21:19:29','09541e1c-1487-4f64-9042-e48da20a075e'),(112,1,'yiyK8MoDi8gAnje-JnKcSftnKFcvGiHpAOk02zAduHzfM6C78z9ntpwHDWQSDYaLGxU7k7KnA9lvUv990WhzbnDsonCrZSgJhDAz','2018-09-17 16:07:59','2018-09-17 16:09:33','b0641485-010c-43a5-931e-f445e0b66004'),(113,1,'Xf8M5vW0c7wbqy4HL_ukYMnQ9FQQ6trj6fvejp5Fk9NgKC8smNOK2F0F4BD0CexgVUykLY9TKGv_RZQ7YKrD_vKTqUP9u7JRKIrf','2018-09-17 19:19:22','2018-09-17 20:26:46','30d23d19-805e-48ff-a615-24b9c3c43a69'),(114,1,'v5OJxG2LEv8TzsdNWLryM_syxVDN_9c_a_5B-tDmfZ03sDT3relIwXj7ZsbvA8Q-PoOM95gYnndq9y3iKHfX5K0PNi4JBkU1Dtqw','2018-09-17 21:57:20','2018-09-17 23:09:26','69ddce7c-0d7f-44bc-bba8-a07c8c417e9b'),(115,1,'4xDsS1lE0TQuZPNCCK6m_F455A3ccQtw11QBMN-pU1WhY-dXX65hmM3ubAsuigh9oRGyGstP3f7J0D9lzUXgb10H1Rh4ME8R6tbd','2018-09-18 00:56:39','2018-09-18 00:56:47','6b61be8a-c2c1-4c36-98a1-bb2854f6b0cb'),(116,1,'cPg_JSzVax473qo44mIRvpwL1uuD3f2hiLwlN3tWTdb3i_YGW6AJZZAv4MJa6MZdhzSls6qzNPI2SP-6-sGK5bbKNyCNoka4goc8','2018-09-18 03:13:46','2018-09-18 03:13:46','a288813f-060d-4cff-8ff3-c3feb1448644'),(117,1,'cBaTTso2v20_F9twURhrwmwxkf5qHrWEI5yKNvoak33f67IE9PUttJJaCUYn4LPo_ngx_03KwJfHFohKBfPIXIGIy_xoEgcI9hAs','2018-09-18 03:13:46','2018-09-18 03:58:31','47cd5558-1c41-4316-8245-04159ecb566c'),(118,1,'C6q0p3c1zJrgoHo5fg2qiZvQfGJTjwhfag1sWovYnTMPIlVIlpoGVqZprTVeu7XjwNS-DmrnfxmeofJfcqkd6lFjpcB4ARaPdHou','2018-09-18 15:44:36','2018-09-18 15:44:36','2b19171a-4204-46c6-a72f-59f0153cfc0e'),(119,1,'LhQMFqOWBZTfl2PF78M77qttw-JgDzFd7xoFp1rtq_G_GKQRuYlzz1luGinVbPxw2g_WNYcm_hM0ncsNNl-NXG1MAWZh2OTMSBf8','2018-09-18 21:14:22','2018-09-18 21:35:30','8e5ec853-9d5f-482f-8a75-5c099b0aed23'),(120,1,'BPSB2w5cXXyNsoZ6N8FJtdjFrkj0jysB6OWIeQszQ3TSWRAwrwp1ajXAjbQVT9F5EmTTQwtw2zxG3A-axGhTnwB_9Z35MQssIzRZ','2018-09-18 22:18:32','2018-09-18 22:18:32','8eac0e26-02c7-422a-8c3f-1e388a3c5fd6'),(121,1,'8W4MdobvxI-8AWA6hQqXaDdR6QyihwNHNce6zZZdUg7EHpWCTf8LMFj0h47vNgjlBTxuaEwtjmF35c3z_FXNlhdq6YYNm6RW1ILC','2018-09-18 23:22:29','2018-09-18 23:22:29','c6c16035-c176-45a3-a863-0ea92c983291'),(122,1,'DNzeT1Zhr1_NeLEbPrLyyPUwodaCAUrBuYeVBFElYEFXn_3mle0lFru78hrbmqqjIv5EllNqPZKvje-C-EBIhN1m-bn9QMKsDjNq','2018-09-18 23:22:29','2018-09-18 23:22:29','d8d118fa-b163-425a-b29b-17acc5683560'),(123,1,'x7oorTL65nO3VP7VbIa3Wa8u58ILTDKnIfphBp5aIM2L-S8cyDF1P9zI2kszAgbGhQ3lypGIPDhculagMGEgdSJd6oPWFflMXRPl','2018-09-19 04:15:19','2018-09-19 04:15:21','b8a539ce-7e56-46f2-bac7-44a20fe9df6b'),(124,1,'JqMlDDx_AXpf7HaBvWgUGkLfzmwABfHtsAHwWM3KBe4uLIyIF10dQo8mGcuBH534SCd05pmJ-jjc9I5T_F57A1NLKw2jL0ac6Z3G','2018-09-19 15:56:42','2018-09-19 15:56:48','b68153b7-bc4c-4d4d-bac8-0dfbd743c99c'),(125,1,'CnIi6kF8uvZIoca0AAHxepBYbePxUcTK1GUq0wECpOSZ-m2eN5NoWKhgYjBN-AGvwuLPnI3Rm92lCW-qPnJ1IbGIkUvDvD-61PeD','2018-09-19 17:27:03','2018-09-19 17:27:03','f00be25c-879f-4c26-941a-8ed495dc42fe'),(126,1,'5VACEVdDXARMCHZsXFNEHhGcbn4Iq7b2A1ulVOx6F3erqTckmYYg9B2LIOy75k6sdyNQddjofxark66ZMGXHQL1449Syv5T3fg4s','2018-09-19 18:18:42','2018-09-19 18:18:42','fd8a5fd1-083b-4daa-8b3c-30be713d7d9f'),(127,1,'QS002g-7QMtVACHgGFSlUSdfMKMfzdZSjU3rGFMReC2OJaaxszTzPyW_LkFt3wnjrTjmLg5kv5dN673D0Y_2hNM1eeoBSnKPdyGP','2018-09-19 18:18:42','2018-09-19 18:18:42','74e60e29-d68c-48d7-8aff-da6da3adcc97'),(128,1,'abhJA8QVk0lBdcvz9SVgMP42vlNYZLBpSji0-mNKk7VYBfiDcfUdILkmUY9tXQfC5w2ZoFjXyOG-ol7Gkyhjhet8mtMkJyL1RBbV','2018-09-19 19:34:11','2018-09-19 19:34:22','02af8ff6-1a15-48ac-bd0b-85221bea82ee'),(129,1,'nx36nHVAn6rFQCQ7luG6b3s2re8UFupuLCSSHz3T4U_jUOnXVOCKanFfsrVjipRziyK8a--17O2-y17wJbOry_afky2s7nHaTkGk','2018-09-19 20:55:42','2018-09-19 20:55:42','1cc759f0-7716-4175-baea-5a982c3fae17'),(130,1,'gq1dD9KItwl_wroWc6zjAtMUEmt9MEnIyMLHdTXrHgz7x2F7-RBH8FAKl7Ry7diW-SK0GIBdgmn8gwj6yUmH6gEzHkWABV72m4Mw','2018-09-19 22:01:12','2018-09-19 22:01:12','ce3f5756-cc31-4de4-97ae-4a8814737ef5');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shunnedmessages`
--

DROP TABLE IF EXISTS `shunnedmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shunnedmessages`
--

LOCK TABLES `shunnedmessages` WRITE;
/*!40000 ALTER TABLE `shunnedmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `shunnedmessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitegroups`
--

DROP TABLE IF EXISTS `sitegroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitegroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sitegroups_name_unq_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitegroups`
--

LOCK TABLES `sitegroups` WRITE;
/*!40000 ALTER TABLE `sitegroups` DISABLE KEYS */;
INSERT INTO `sitegroups` VALUES (1,'Jeremy Evans','2018-08-16 22:24:19','2018-08-16 22:24:19','d975c01f-dc03-4b35-886e-41c0b0ee9092');
/*!40000 ALTER TABLE `sitegroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `language` varchar(12) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sites_handle_unq_idx` (`handle`),
  KEY `sites_sortOrder_idx` (`sortOrder`),
  KEY `sites_groupId_fk` (`groupId`),
  CONSTRAINT `sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `sitegroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites`
--

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;
INSERT INTO `sites` VALUES (1,1,1,'Jeremy Evans','default','en-US',1,'@web/',1,'2018-08-16 22:24:19','2018-08-16 22:24:19','c43c0f81-8e74-4b52-b989-429329350b08');
/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stc_1_leftimage`
--

DROP TABLE IF EXISTS `stc_1_leftimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stc_1_leftimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stc_1_leftimage_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `stc_1_leftimage_siteId_fk` (`siteId`),
  CONSTRAINT `stc_1_leftimage_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stc_1_leftimage_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stc_1_leftimage`
--

LOCK TABLES `stc_1_leftimage` WRITE;
/*!40000 ALTER TABLE `stc_1_leftimage` DISABLE KEYS */;
INSERT INTO `stc_1_leftimage` VALUES (1,39,1,'2018-08-19 09:18:33','2018-08-19 09:18:33','3cb02020-52bc-4b0d-80a2-2bee18bd1060');
/*!40000 ALTER TABLE `stc_1_leftimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stc_1_rightimage`
--

DROP TABLE IF EXISTS `stc_1_rightimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stc_1_rightimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stc_1_rightimage_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `stc_1_rightimage_siteId_fk` (`siteId`),
  CONSTRAINT `stc_1_rightimage_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stc_1_rightimage_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stc_1_rightimage`
--

LOCK TABLES `stc_1_rightimage` WRITE;
/*!40000 ALTER TABLE `stc_1_rightimage` DISABLE KEYS */;
INSERT INTO `stc_1_rightimage` VALUES (1,40,1,'2018-08-19 09:18:33','2018-08-19 09:18:33','e1d83f01-3be3-41d6-b1cc-8b778dd8c85a');
/*!40000 ALTER TABLE `stc_1_rightimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stc_awards`
--

DROP TABLE IF EXISTS `stc_awards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stc_awards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_awards` text,
  `field_category` text,
  `field_year` text,
  `field_awardsUrl` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stc_awards_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `stc_awards_siteId_fk` (`siteId`),
  CONSTRAINT `stc_awards_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stc_awards_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stc_awards`
--

LOCK TABLES `stc_awards` WRITE;
/*!40000 ALTER TABLE `stc_awards` DISABLE KEYS */;
INSERT INTO `stc_awards` VALUES (1,10,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','2c7bb007-21cf-4bf0-8193-5a41ad734e98','Large Brand Identity','Gold (x2)','2016',NULL),(2,11,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','b42342a4-cfbe-48ac-b383-b2deda5526bc','Business Communication','Gold','2016',NULL),(3,12,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','f59e040a-42dd-4bf5-bced-873f8f58e9ec','Colour Award','Silver (x1) Bronze (x1)','2016',NULL),(4,13,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','795850aa-1262-45be-8ba2-c3d596623766','Design Communication','Silver','2016',NULL),(5,14,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','72d6d681-0e62-44cf-acfe-fdb906e7eef8','Design Craft','Silver','2016',NULL),(6,15,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','3fa0f63a-4260-4c07-871f-178f051c94d2','Small Scale Websites','Silver','2016',NULL),(7,16,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','03cae4d9-6289-4ee4-8d1e-266767a4442d','Environmental Graphics','Bronze','2016',NULL),(8,17,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','b4efa834-c4d6-4dce-b394-e191cb63bcba','Self Promotion','Bronze','2016',NULL),(9,18,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','416c6ff1-187c-487f-8bef-988c7c0ab4d8','Small Brand Identity','Bronze (x3)','2016',NULL);
/*!40000 ALTER TABLE `stc_awards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stc_experience`
--

DROP TABLE IF EXISTS `stc_experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stc_experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_experience` text,
  `field_position` text,
  `field_period` text,
  `field_experienceUrl` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stc_experience_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `stc_experience_siteId_fk` (`siteId`),
  CONSTRAINT `stc_experience_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stc_experience_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stc_experience`
--

LOCK TABLES `stc_experience` WRITE;
/*!40000 ALTER TABLE `stc_experience` DISABLE KEYS */;
INSERT INTO `stc_experience` VALUES (1,6,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','6b45f808-e5e0-4434-8ce6-7ca29d129dc9','Studio South','Design Director','09.14 – 07.18','http://studiosouth.co.nz'),(2,7,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','955a829e-5fa2-4584-86a2-c97bc5c3ae18','Kurppa Hosk','Freelance Designer','09.14 – 07.18',NULL),(3,8,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','e4b6bf54-c47c-48e6-96e7-f35b01972f61','Tre Kronor Media','Graphic Designer','09.14 – 07.18',NULL),(4,9,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','bf8de95f-d166-4dc5-b6dc-760e51c30537','ATNA','Graphic Designer','09.14 – 07.18',NULL);
/*!40000 ALTER TABLE `stc_experience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stc_heroimage`
--

DROP TABLE IF EXISTS `stc_heroimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stc_heroimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stc_heroimage_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `stc_heroimage_siteId_fk` (`siteId`),
  CONSTRAINT `stc_heroimage_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `stc_heroimage_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stc_heroimage`
--

LOCK TABLES `stc_heroimage` WRITE;
/*!40000 ALTER TABLE `stc_heroimage` DISABLE KEYS */;
INSERT INTO `stc_heroimage` VALUES (1,22,1,'2018-08-18 22:32:47','2018-08-21 08:15:58','6b93fd5f-5cfa-4bc7-9948-785c0f95791e'),(2,24,1,'2018-08-18 22:32:57','2018-08-19 09:12:55','db76d907-b226-48f6-9b2a-e8eba60b27e6'),(3,26,1,'2018-08-18 22:33:35','2018-08-19 09:12:55','62a37b32-c285-4e72-bbd8-dee438c7822e'),(4,28,1,'2018-08-18 22:33:57','2018-08-19 09:12:55','ef89a82d-a673-4ee9-a658-ef62be423c0a'),(5,30,1,'2018-08-18 22:34:21','2018-08-19 09:12:55','4880fd5e-fbdc-495d-8a8b-89a22d9bc4fa'),(6,32,1,'2018-08-18 22:34:53','2018-08-19 09:12:55','c480eaed-747c-4a73-ab16-0e13636b1054'),(7,34,1,'2018-08-18 22:35:10','2018-08-19 09:12:55','f22cf70b-3642-453e-8400-c3de294ba7fd'),(8,36,1,'2018-08-18 22:35:44','2018-08-19 09:12:55','9174acc3-1189-4490-914d-2121925771a3');
/*!40000 ALTER TABLE `stc_heroimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structureelements`
--

DROP TABLE IF EXISTS `structureelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `structureelements_root_idx` (`root`),
  KEY `structureelements_lft_idx` (`lft`),
  KEY `structureelements_rgt_idx` (`rgt`),
  KEY `structureelements_level_idx` (`level`),
  KEY `structureelements_elementId_idx` (`elementId`),
  CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structureelements`
--

LOCK TABLES `structureelements` WRITE;
/*!40000 ALTER TABLE `structureelements` DISABLE KEYS */;
INSERT INTO `structureelements` VALUES (1,1,NULL,1,1,6,0,'2018-08-16 23:23:48','2018-08-16 23:23:59','d3d3e446-3570-4c2f-b90f-2618a502d75b'),(2,1,4,1,2,3,1,'2018-08-16 23:23:48','2018-08-16 23:23:48','ba9c400a-4ec8-4074-a0cf-565a02059de1'),(3,1,5,1,4,5,1,'2018-08-16 23:23:59','2018-08-16 23:23:59','22895808-21e5-4d81-8371-8b8f33dec035'),(4,2,NULL,4,1,18,0,'2018-08-18 22:32:47','2018-08-18 22:35:44','7829ea21-090b-4603-8007-71f427a76c80'),(5,2,21,4,2,3,1,'2018-08-18 22:32:47','2018-08-18 22:32:47','8ade04d1-fdea-4559-847f-0318dfbdcafe'),(6,2,23,4,4,5,1,'2018-08-18 22:32:57','2018-08-18 22:32:57','a719a4ef-652b-433a-9b25-0a09a9b2b4f2'),(7,2,25,4,6,7,1,'2018-08-18 22:33:35','2018-08-18 22:33:35','77b770d8-23fc-4462-a494-ff7c1633f1e0'),(8,2,27,4,8,9,1,'2018-08-18 22:33:57','2018-08-18 22:33:57','87942431-efe3-436d-be55-fdf26a5bf650'),(9,2,29,4,10,11,1,'2018-08-18 22:34:21','2018-08-18 22:34:21','1229dbde-b54a-4566-a0de-dc65b08811de'),(10,2,31,4,12,13,1,'2018-08-18 22:34:53','2018-08-18 22:34:53','a95c4f65-f3a0-44c5-ab7b-58d4614df598'),(11,2,33,4,14,15,1,'2018-08-18 22:35:10','2018-08-18 22:35:10','ba8f79f4-5408-460f-8477-f4257e799ab8'),(12,2,35,4,16,17,1,'2018-08-18 22:35:44','2018-08-18 22:35:44','c1acf2ea-0fab-4f4f-9f4d-220669bde3ef');
/*!40000 ALTER TABLE `structureelements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structures`
--

DROP TABLE IF EXISTS `structures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structures`
--

LOCK TABLES `structures` WRITE;
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;
INSERT INTO `structures` VALUES (1,1,'2018-08-16 23:22:39','2018-08-16 23:22:39','6358723f-9ed3-43d3-8011-8f9de137d3ab'),(2,1,'2018-08-18 22:25:58','2018-08-18 22:25:58','c9201c63-3afa-4994-8d9d-a6051bc7c6a2');
/*!40000 ALTER TABLE `structures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supertableblocks`
--

DROP TABLE IF EXISTS `supertableblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supertableblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `ownerSiteId` int(11) DEFAULT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `supertableblocks_ownerId_idx` (`ownerId`),
  KEY `supertableblocks_fieldId_idx` (`fieldId`),
  KEY `supertableblocks_typeId_idx` (`typeId`),
  KEY `supertableblocks_ownerSiteId_idx` (`ownerSiteId`),
  CONSTRAINT `supertableblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `supertableblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `supertableblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `supertableblocks_ownerSiteId_fk` FOREIGN KEY (`ownerSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `supertableblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `supertableblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supertableblocks`
--

LOCK TABLES `supertableblocks` WRITE;
/*!40000 ALTER TABLE `supertableblocks` DISABLE KEYS */;
INSERT INTO `supertableblocks` VALUES (6,2,NULL,6,1,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','8b882a45-212d-4061-9f7f-d72637094ec8'),(7,2,NULL,6,1,2,'2018-08-17 01:03:14','2018-08-17 06:02:39','899bb471-f920-4d15-ba84-1a58bc0b757c'),(8,2,NULL,6,1,3,'2018-08-17 01:03:14','2018-08-17 06:02:39','aaa1522e-805f-4dea-bc95-c5425973b9d1'),(9,2,NULL,6,1,4,'2018-08-17 01:03:14','2018-08-17 06:02:39','597638c6-b2c3-4093-bf62-8463e73c15e5'),(10,2,NULL,10,2,1,'2018-08-17 01:03:14','2018-08-17 06:02:39','acc03f8e-3623-4bf4-9dcf-16d4e60b39bd'),(11,2,NULL,10,2,2,'2018-08-17 01:03:14','2018-08-17 06:02:39','41bf4f6b-066f-4035-a9d7-c0697df34b23'),(12,2,NULL,10,2,3,'2018-08-17 01:03:14','2018-08-17 06:02:39','949bc82d-c461-4f89-825e-77ab1c23d416'),(13,2,NULL,10,2,4,'2018-08-17 01:03:14','2018-08-17 06:02:39','6a1d68ae-48cd-4565-a4fd-e74ce74198f3'),(14,2,NULL,10,2,5,'2018-08-17 01:03:14','2018-08-17 06:02:39','058539c1-4105-4fa6-9921-64e6d1d4a5fa'),(15,2,NULL,10,2,6,'2018-08-17 01:03:14','2018-08-17 06:02:39','de806a92-4edd-45a7-b49c-75350d637d53'),(16,2,NULL,10,2,7,'2018-08-17 01:03:14','2018-08-17 06:02:39','a02b9655-0eda-4f59-8279-a491f68fcbb1'),(17,2,NULL,10,2,8,'2018-08-17 01:03:14','2018-08-17 06:02:39','502ced5a-a2a0-4597-8433-cdc80ffe0254'),(18,2,NULL,10,2,9,'2018-08-17 01:03:14','2018-08-17 06:02:39','c16cba66-51a4-4a50-a5a0-8b7e6b4dc710'),(22,21,NULL,18,3,1,'2018-08-18 22:32:47','2018-08-21 08:15:58','c6e2b80a-cfe7-477c-8789-950bce2a3022'),(24,23,NULL,18,3,1,'2018-08-18 22:32:57','2018-08-19 09:12:55','1f990db5-2f2a-44df-a8bb-0b5fadcb068f'),(26,25,NULL,18,3,1,'2018-08-18 22:33:35','2018-08-19 09:12:55','c0b598f4-01ba-4ec1-8068-c9c4b5a1198d'),(28,27,NULL,18,3,1,'2018-08-18 22:33:57','2018-08-19 09:12:55','d3373211-f9b9-42a3-a963-9ab583894827'),(30,29,NULL,18,3,1,'2018-08-18 22:34:21','2018-08-19 09:12:55','52c9a693-c5cb-4948-ab8e-e7beb4688905'),(32,31,NULL,18,3,1,'2018-08-18 22:34:53','2018-08-19 09:12:55','9b1183a5-950b-41d9-8308-2707ade857bf'),(34,33,NULL,18,3,1,'2018-08-18 22:35:10','2018-08-19 09:12:55','4a2be781-4ffa-4d62-b6a4-92cc847f9a88'),(36,35,NULL,18,3,1,'2018-08-18 22:35:44','2018-08-19 09:12:55','e98a8da2-9c5f-4eff-a7f9-3997fb4d569f'),(39,37,NULL,22,4,1,'2018-08-19 09:18:33','2018-08-19 09:18:33','7446e053-442f-4c18-9407-844ab60021c1'),(40,37,NULL,24,5,1,'2018-08-19 09:18:33','2018-08-19 09:18:33','1d011fc1-5b41-4557-81fe-51d77248d29d');
/*!40000 ALTER TABLE `supertableblocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supertableblocktypes`
--

DROP TABLE IF EXISTS `supertableblocktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supertableblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `supertableblocktypes_fieldId_idx` (`fieldId`),
  KEY `supertableblocktypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `supertableblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `supertableblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supertableblocktypes`
--

LOCK TABLES `supertableblocktypes` WRITE;
/*!40000 ALTER TABLE `supertableblocktypes` DISABLE KEYS */;
INSERT INTO `supertableblocktypes` VALUES (1,6,4,'2018-08-17 00:55:12','2018-08-17 06:01:15','2278f76c-d4a2-49bd-9490-6dc874652e27'),(2,10,5,'2018-08-17 00:56:16','2018-08-17 06:01:28','34e8ad62-2bf2-44a9-adcc-a1963e5bfb55'),(3,18,6,'2018-08-18 22:25:09','2018-08-21 08:22:19','091adf4f-fa5b-43b1-bcc4-7bd95ab69897'),(4,22,9,'2018-08-19 09:12:36','2018-08-19 09:18:19','f497a3b4-ce74-42e0-9f03-c3410f9169d1'),(5,24,10,'2018-08-19 09:12:36','2018-08-19 09:18:19','1e2bae05-11bf-47fc-8bb8-66cd1940d7e1');
/*!40000 ALTER TABLE `supertableblocktypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemmessages`
--

DROP TABLE IF EXISTS `systemmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemmessages_key_language_unq_idx` (`key`,`language`),
  KEY `systemmessages_language_idx` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemmessages`
--

LOCK TABLES `systemmessages` WRITE;
/*!40000 ALTER TABLE `systemmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemmessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemsettings`
--

DROP TABLE IF EXISTS `systemsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemsettings`
--

LOCK TABLES `systemsettings` WRITE;
/*!40000 ALTER TABLE `systemsettings` DISABLE KEYS */;
INSERT INTO `systemsettings` VALUES (1,'email','{\"fromEmail\":\"hi@graphicactivity.co.nz\",\"fromName\":\"Jeremy Evans\",\"transportType\":\"craft\\\\mail\\\\transportadapters\\\\Sendmail\"}','2018-08-16 22:24:20','2018-08-16 22:24:20','64ed81dc-99a3-441e-b4f0-c15904d9ace2');
/*!40000 ALTER TABLE `systemsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taggroups`
--

DROP TABLE IF EXISTS `taggroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `taggroups_handle_unq_idx` (`handle`),
  KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taggroups`
--

LOCK TABLES `taggroups` WRITE;
/*!40000 ALTER TABLE `taggroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `taggroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tags_groupId_idx` (`groupId`),
  CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecacheelements`
--

DROP TABLE IF EXISTS `templatecacheelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `templatecacheelements_cacheId_idx` (`cacheId`),
  KEY `templatecacheelements_elementId_idx` (`elementId`),
  CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecacheelements`
--

LOCK TABLES `templatecacheelements` WRITE;
/*!40000 ALTER TABLE `templatecacheelements` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecacheelements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecachequeries`
--

DROP TABLE IF EXISTS `templatecachequeries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecachequeries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecachequeries_cacheId_idx` (`cacheId`),
  KEY `templatecachequeries_type_idx` (`type`),
  CONSTRAINT `templatecachequeries_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecachequeries`
--

LOCK TABLES `templatecachequeries` WRITE;
/*!40000 ALTER TABLE `templatecachequeries` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecachequeries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecaches`
--

DROP TABLE IF EXISTS `templatecaches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecaches_cacheKey_siteId_expiryDate_path_idx` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  KEY `templatecaches_cacheKey_siteId_expiryDate_idx` (`cacheKey`,`siteId`,`expiryDate`),
  KEY `templatecaches_siteId_idx` (`siteId`),
  CONSTRAINT `templatecaches_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecaches`
--

LOCK TABLES `templatecaches` WRITE;
/*!40000 ALTER TABLE `templatecaches` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecaches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) NOT NULL,
  `route` text,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tokens_token_unq_idx` (`token`),
  KEY `tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroups`
--

DROP TABLE IF EXISTS `usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_handle_unq_idx` (`handle`),
  UNIQUE KEY `usergroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroups`
--

LOCK TABLES `usergroups` WRITE;
/*!40000 ALTER TABLE `usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroups_users`
--

DROP TABLE IF EXISTS `usergroups_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `usergroups_users_userId_idx` (`userId`),
  CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroups_users`
--

LOCK TABLES `usergroups_users` WRITE;
/*!40000 ALTER TABLE `usergroups_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroups_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions`
--

DROP TABLE IF EXISTS `userpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions`
--

LOCK TABLES `userpermissions` WRITE;
/*!40000 ALTER TABLE `userpermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions_usergroups`
--

DROP TABLE IF EXISTS `userpermissions_usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `userpermissions_usergroups_groupId_idx` (`groupId`),
  CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions_usergroups`
--

LOCK TABLES `userpermissions_usergroups` WRITE;
/*!40000 ALTER TABLE `userpermissions_usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions_usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions_users`
--

DROP TABLE IF EXISTS `userpermissions_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `userpermissions_users_userId_idx` (`userId`),
  CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions_users`
--

LOCK TABLES `userpermissions_users` WRITE;
/*!40000 ALTER TABLE `userpermissions_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpreferences`
--

DROP TABLE IF EXISTS `userpreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpreferences` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `preferences` text,
  PRIMARY KEY (`userId`),
  CONSTRAINT `userpreferences_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpreferences`
--

LOCK TABLES `userpreferences` WRITE;
/*!40000 ALTER TABLE `userpreferences` DISABLE KEYS */;
INSERT INTO `userpreferences` VALUES (1,'{\"language\":\"en-US\"}');
/*!40000 ALTER TABLE `userpreferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unq_idx` (`username`),
  UNIQUE KEY `users_email_unq_idx` (`email`),
  KEY `users_uid_idx` (`uid`),
  KEY `users_verificationCode_idx` (`verificationCode`),
  KEY `users_photoId_fk` (`photoId`),
  CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_photoId_fk` FOREIGN KEY (`photoId`) REFERENCES `assets` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Jeremy',NULL,NULL,NULL,'hi@graphicactivity.co.nz','$2y$13$oUSqtrl2yEzfEh0cDshHmu9IkLvdo6LpIe3HA2ms.qCPGR1Pa/dkq',1,0,0,0,'2018-09-19 22:01:12','73.202.93.178',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'2018-08-16 22:24:20','2018-08-16 22:24:20','2018-09-19 22:01:12','ea503f4d-9b76-4ae3-a427-b2bf08d2f43a');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volumefolders`
--

DROP TABLE IF EXISTS `volumefolders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumefolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `volumefolders_name_parentId_volumeId_unq_idx` (`name`,`parentId`,`volumeId`),
  KEY `volumefolders_parentId_idx` (`parentId`),
  KEY `volumefolders_volumeId_idx` (`volumeId`),
  CONSTRAINT `volumefolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `volumefolders_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volumefolders`
--

LOCK TABLES `volumefolders` WRITE;
/*!40000 ALTER TABLE `volumefolders` DISABLE KEYS */;
INSERT INTO `volumefolders` VALUES (1,NULL,1,'Uploads','','2018-08-18 22:27:12','2018-08-18 22:27:12','2294e741-697a-4eea-884a-58210c57c550'),(2,NULL,NULL,'Temporary source',NULL,'2018-08-18 22:32:02','2018-08-18 22:32:02','7e1f1825-63d4-4623-9d29-19aa9d36fe16'),(3,2,NULL,'user_1','user_1/','2018-08-18 22:32:02','2018-08-18 22:32:02','b818825c-54fc-4b01-88be-66a8e4d54a4e');
/*!40000 ALTER TABLE `volumefolders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volumes`
--

DROP TABLE IF EXISTS `volumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `settings` text,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `volumes_name_unq_idx` (`name`),
  UNIQUE KEY `volumes_handle_unq_idx` (`handle`),
  KEY `volumes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `volumes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volumes`
--

LOCK TABLES `volumes` WRITE;
/*!40000 ALTER TABLE `volumes` DISABLE KEYS */;
INSERT INTO `volumes` VALUES (1,8,'Uploads','uploads','craft\\volumes\\Local',1,'/uploads','{\"path\":\"uploads/\"}',1,'2018-08-18 22:27:12','2018-08-18 22:27:12','a043d6a6-0ec3-4bc0-aeee-070f1f5f13ee');
/*!40000 ALTER TABLE `volumes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widgets`
--

DROP TABLE IF EXISTS `widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `widgets_userId_idx` (`userId`),
  CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widgets`
--

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;
INSERT INTO `widgets` VALUES (1,1,'craft\\widgets\\RecentEntries',1,0,'{\"section\":\"*\",\"siteId\":\"1\",\"limit\":10}',1,'2018-08-16 22:25:10','2018-08-16 22:25:10','ad0b13b7-f975-47d2-b83b-820b8452bb55');
/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-29 19:50:52

// Require

var gulp    = require('gulp'),
    sass    = require('gulp-sass'),
    concat  = require('gulp-concat'),
    prefix  = require('gulp-autoprefixer'),
    postcss = require('gulp-postcss'),
    rename  = require('gulp-rename'),
    uglify  = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps');


gulp.task('sass', function(){
  return gulp.src('assets/scss/**/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(sourcemaps.write())
  .pipe(concat('site.min.css'))
  .pipe(prefix({
            browsers: ['last 2 versions'],
            cascade: false
        }))
  .pipe(gulp.dest('web/assets/css/'));
});

gulp.task('bootstrap', function(){
  return gulp.src('assets/bootstrap-scss/**/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(sourcemaps.write())
  .pipe(concat('bootstrap-grid.min.css'))
  .pipe(prefix({
            browsers: ['last 2 versions'],
            cascade: false
        }))
  .pipe(gulp.dest('web/assets/css/'));
});

gulp.task('scripts', function() {
  return gulp.src(['assets/js/**/*.js'])
  .pipe(concat('main.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('web/assets/js'));
});

gulp.task('watch', function () {
  gulp.watch('assets/scss/**/*.scss', ['sass']);
  gulp.watch('assets/bootstrap-scss/**/*.scss', ['bootstrap']);
  gulp.watch('assets/js/*.js', ['scripts']);
});

$(document).ready(function(){
    // grid/list toggle click
    $(".gridLabel").click(function(event) {
        $(this).addClass("toggleUnderline");
        $(".listLabel").removeClass("toggleUnderline");
        $(".workRows").addClass("hideList");
        $(".gridImg").addClass("showGrid");
    });
    $(".listLabel").click(function(event) {
        $(this).addClass("toggleUnderline");
        $(".gridLabel").removeClass("toggleUnderline");
        $(".workRows").removeClass("hideList");
        $(".gridImg").removeClass("showGrid");
    });
    // Mobile icon
    $(".menu").click(function(event) {
        $(".mobileMenu").toggleClass("openMenu");
    });
    // hide img
    $(".tableImg").hide();
    // show img
    $(".tableWorkLink").mouseover(function(event) {
        $(".tableImg").hide();
        var relatedDivID = $(this).attr('id');

        $("" + relatedDivID).toggle();
    });
    // hide img
    $(".tableWorkLink").mouseout(function(event) {
        $(".tableImg").hide();
    });
    // summary
    $("#summary").click(function(event) {
        $(this).text(function(i, text) {
            return text === "Project Info –" ? "Project Info +" : "Project Info –";
        })
        $(".projectSummary").toggleClass("showSummary");
        //$("body").toggleClass("compensate-for-scrollbar");
    });
    /*$("#closeSummary").click(function(event) {
        $(".projectSummary").toggleClass("showSummary");
    });*/
    // And with esc
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // esc keycode
            $(".projectSummary").removeClass("showSummary");
            $("body").removeClass("compensate-for-scrollbar");
        }
    });
    // fancybox
    /*$('[data-fancybox="gallery"]').fancybox({
        loop : true,
        autoStart : false,
        animationEffect : false,
	    clickContent : false,
    });*/
    var fancyGallery = $(".workGallery").find("a");
    fancyGallery.attr("rel","gallery").fancybox({
       type: "image"
    });
    $('#view').on('click', function() {
       fancyGallery.eq(0).click();
   });
});
